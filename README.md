This portfolio contains projects developed in various languages and frameworks such python, Java, Express.js, HTML5, CSS, Vue.js and Node.js.

Each project folder contains a **README.md** that provides an overview of the project as well as instructions on how to run the program.