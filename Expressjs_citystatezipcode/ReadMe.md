﻿## Assignment #2

Author:	Karn Kausal

Class:		MET CS 602

Date:		1/20/2022



## Assignment Summary/Description

**Part 1:**  The clients communicate with the server using the following commands: 
* lookupByZipCode, <zip> 
* lookupByCityState, <city,state>
* getPopulationByState, <state>
The **server** application listens for client connections and when the client command is received, processes the request by invoking the corresponding function available through the zipCodeModule. The server then uses JSON.stringify to send the result back to the client.
The **client** application reads commands from the user’s console and sends the commands to the server. When the data is received back from the server, the corresponding result is printed to the console.

**Part 2:**  The Express server application (server.js) and the corresponding views were rendered using Handlebars.  Various route handlers were implemented for GET and POST and the request object's *query* as well as *params* properties were used to process the data from the clients.  For the latter, some GET requests were implemented to ensure they can handle JSON, XML and HTML requests.


## Assumptions

Following assumptions were made during the design and development phase:

- It was assumed acceptable to make use of methods such as the split method from the String object.
- Similar to the lecture notes example (ex09_rest.js),  it was assumed that xml (for Part 2) can be configured as shown in this lecture notes example.

## Program Execution
- Execution of the program requires the following to be run via a terminal program (or command prompt in Windows):
	- Execute the "***npm install***" command in the respective directories (part1 and part2) where the server.js and client.js files reside.  This will install the dependencies defined in the package.json.
	- Execute the command "***node server.js***"   to launch the server and to launch client connections, launch the command "***node client.js***".  (Note:  The server should be launched prior to launching client connections.)

## Conclusion:
All code was tested prior to submittal and is functional.  In addition to testing browser connections, the REST endpoints for JSON and XML data were tested using curl as well as Postman and all tests passed successfully.  
