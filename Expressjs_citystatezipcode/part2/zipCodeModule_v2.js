/**
 * Assignment: 1
 * Author: Karn Kausal
 * MET CS 602
 */

const data = require('./zips.json');

module.exports.lookupByZipCode = (zip) => {
    return data.find(element => (element._id === zip));
};

module.exports.lookupByCityState = (city, state) => {
    return cityStateArr = data.filter(element =>
        (element.state === state && element.city === city)).map((element) => {
            // return the array that matches the city and state.        
            let tempObj     = {};
            tempObj.zip     = element._id;
            tempObj.pop     = element.pop;
            return tempObj;
            

        });
    

};

module.exports.getPopulationByState = (state) => {
    // reference:  https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/Reduce

    let totalPopSum = data.reduce((previousValue, currentValue) => {
        if (currentValue.state.toLowerCase() === state.toLowerCase()) {
            return previousValue + currentValue.pop;
        }
        else {
            return previousValue;
        }

    }, 0);
    return totalPopSum;
};

