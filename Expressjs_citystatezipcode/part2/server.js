const express = require('express');
const app = express();

const bodyParser = require("body-parser");
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// setup handlebars view engine
const handlebars = require('express-handlebars');

app.engine('handlebars', 
	handlebars({defaultLayout: 'main'}));

app.set('view engine', 'handlebars');

// static resources
app.use(express.static(__dirname + '/public'));

// Use the zipCode module
const cities = require('./zipCodeModule_v2');

// GET request to the homepage
// GET request – / : Render the home view with a welcome message.
app.get('/',  (req, res) => {
	res.render('homeView');
});

/***************	GET request – /zip	*************************/
// If the request query id parameter is present, lookup the corresponding 
// data and render the lookupByZipView.
// Otherwise, render the lookupByZipForm.
app.get('/zip', (req, res) => {
	
	// If the request query id parameter is present, render the appropriate view.
	if (req.query.id) {
		// lookup zipcode per request query id parameter
		const zipInfo = cities.lookupByZipCode(req.query.id);
		res.render('lookupByZipView', {
			zipCode: zipInfo._id,
			city: zipInfo.city,
			state: zipInfo.state,
			population: zipInfo.pop
		});
	}
	else {
		res.render('lookupByZipForm');
	}	
});

/***************	POST request – /zip	*************************/
// Lookup the corresponding data for the request body id parameter and 
// render the lookupByZipView.
app.post('/zip', (req, res) => {

	// look up zipcode per request body id parameter.
	const zipInfo = cities.lookupByZipCode(req.body.id);

	res.render('lookupByZipView', {
		zipCode: zipInfo._id,
		city: zipInfo.city,
		state: zipInfo.state,
		population: zipInfo.pop
	});	

});

/***************	GET request – /zip/:id 	*********************/
// Should be capable of handling json, xml, and html requests.
// Use the named routing id parameter and lookup the corresponding data.
// For html request, render the lookupByZipView.

app.get('/zip/:id', (req, res) => {
	// retrieve/lookup zipcode
	let retrievedZipCode = cities.lookupByZipCode(req.params.id.split(';')[0]);
	
	// Implement the JSON, XML, & HTML formats
	res.format({
		'application/json': ()=> {
			// res.send("Accessing json zip...");
			res.json(retrievedZipCode);
		},

		'application/xml': () => {		
			let zipXml =	
			'<?xml version="1.0"?>\n' +
				'<zipCode id="' + retrievedZipCode._id + '">\n' 		+
				'   <city>' + retrievedZipCode.city 	+ '</city>\n' 	+
				'   <state>' + retrievedZipCode.state 	+ '</state>\n' 	+
				'   <pop>' + retrievedZipCode.pop 		+ '</pop>\n' 	+
				'</zipcode>';

			res.type('application/xml');
			res.send(zipXml);
		},

		'text/html': () => {
			res.render('lookupByZipView', {
				zipCode: retrievedZipCode._id,
				city: retrievedZipCode.city,
				state: retrievedZipCode.state,
				population: retrievedZipCode.pop
			});	
		}
	});

});

/***************	GET request – /city  	*********************/
// If the request query city parameter and state parameter are present, 
// lookup the corresponding data and render the 
// lookupByCityStateView.
// Otherwise, render the lookupByCityStateForm.
app.get('/city', (req, res) => {

	// If the request query id parameter is present, render the appropriate view.
	if (req.query.city && req.query.state) {
		// lookup info per request query id parameter
		const retrievedCityState = cities.lookupByCityState(req.query.city, req.query.state);
		res.render('lookupByCityStateView', {			
			city: req.query.city,
			state: req.query.state,			
			allCityStateInfo: retrievedCityState
		});
	}
	else {
		res.render('lookupByCityStateForm');
	}	
	
	
});

/***************	POST request – /city   	*********************/
// Lookup the corresponding data for the request body state and city
// parameters and render the lookupByCityStateView.
app.post('/city', (req, res) => {
	
	// look up city & state per request body id parameter.
	const zipPopForCityState = cities.lookupByCityState(req.body.city.toUpperCase(), req.body.state.toUpperCase());

	res.render('lookupByCityStateView', {			
		city: req.body.city.toUpperCase(),
		state: req.body.state.toUpperCase(),			
		allCityStateInfo: zipPopForCityState
	});

});


/***************	GET request – /city/:city/state/:state ******/
// Should be capable of handling json, xml, and html requests.
// Use the named routing city and state parameters and lookup the 
// corresponding data.
// For html request, render the lookupByCityStateView.

app.get('/city/:city/state/:state', (req, res) => {
	
	// retrieve/lookup city and state info
	let zipPopForCityState = cities.lookupByCityState(req.params.city.toUpperCase(), req.params.state.toUpperCase().split(';')[0]);
	
	// Implement the JSON, XML, & HTML formats
	res.format({
		'application/json': ()=> {
			// res.send("Accessing json zip...");
			let tempJson = {"city":req.params.city.toUpperCase(), "state": req.params.state.toUpperCase().split(';')[0], "data": zipPopForCityState };
			res.json(tempJson);
		},

		'application/xml': () => {		
			let zipPopXml =	
			'<?xml version="1.0"?>\n' +
				'<city-state city="' + req.params.city + ' state="' + req.params.state + '">\n';
				for (let i = 0; i < zipPopForCityState.length; i++) { 
					zipPopXml += '  <entry zip="' + zipPopForCityState[i].zip + '" pop="' + zipPopForCityState[i].pop + '" />\n';
				}
				zipPopXml += '</city-state>';
			res.type('application/xml');
			res.send(zipPopXml);
		},

		'text/html': () => {
			res.render('lookupByCityStateView', {			
				city: req.params.city,
				state: req.params.state,			
				allCityStateInfo: zipPopForCityState
			});	
		}
	});

});

/***************	GET request – /pop    	*********************/
// If the request query state parameter is present, lookup the corresponding 
// data and render the populationView.
// Otherwise, render the PopulationForm.
app.get('/pop', (req, res) => {

	// If the request query state parameter is present, render the appropriate view.
	if (req.query.state) {
		// lookup info per request query state parameter
		const popInfo = cities.getPopulationByState(req.query.state);
		res.render('populationView', {
			state: req.query.state,
			population: popInfo
		});
	}
	else {
		res.render('populationForm');
	}	
});

/***************	GET request – /pop/:state     	*************/ 
// Should be capable of handling json, xml, and html requests 
// Use the named routing state parameter and lookup the corresponding 
// data.
// For html request, render the populationView.

app.get('/pop/:state', (req, res) => {
	
	// retrieve/lookup pop info
	let popForState = cities.getPopulationByState(req.params.state.split(';')[0]);

	// Implement the JSON, XML, & HTML formats
	res.format({
		'application/json': ()=> {
			let tempJson = {"state": req.params.state.toUpperCase().split(';')[0], "pop": popForState};
			res.json(tempJson);
		},

		'application/xml': () => {		
			let popXml =	
			'<?xml version="1.0"?>\n' +
				'<state-pop state="' + req.params.state.split(';')[0] + '">\n' +
				'  <pop>' + popForState + '</pop>\n' +
				'</state-pop>';
				
			res.type('application/xml');
			res.send(popXml);
		},

		'text/html': () => {
			res.render('populationView', {
				state: req.params.state.split(';')[0],
				population: popForState
			});
		}
	});
});


app.use((req, res) => {
	res.status(404);
	res.render('404');
});

app.listen(3000, () => {
  console.log('http://localhost:3000');
});




