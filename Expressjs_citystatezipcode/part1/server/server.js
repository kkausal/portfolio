const net = require('net');

const colors = require('colors');

const cities = require('./zipCodeModule_v2');

const server = net.createServer((socket) => {

	console.log("Client connection...".red);

	socket.on('end', () => {
		console.log("Client disconnected...".red);
	});

	// HW Code - Write the following code to process data from client
	
	socket.on('data', (data) => {

		let input = data.toString();
		console.log(colors.blue('...Received %s'), input);
		


		// Fill in the rest

		// split string on the comma delimiter to determine which client command was recieved and
		// extract the function name and declare variables for the args provided after the function name.
		let jsonToClient;
		const splitArr 			= input.split(',');
		const functionName		= splitArr[0];
		let firstArg;
		let secondArg;
		let str;
		try {
			switch (functionName.toLowerCase()) {
				case 'lookupbyzipcode':
					firstArg		= splitArr[1].trim();
					jsonToClient	= cities.lookupByZipCode(firstArg);					
					break;
				case 'lookupbycitystate':					
					firstArg 		= splitArr[1].trim();
					secondArg 		= splitArr[2].trim();
					// throw exception if value is falsy
					// falsy values in JS are as follows:  false, 0, "", null, undefined, and NaN).
					// in this case, if empty string or "", value is falsy and exception is thrown so
					// that output is "Invalid Request."
					if(!secondArg)
						throw 'errorEmptyString';
					jsonToClient 	= {"city": firstArg, "state": secondArg, "data": cities.lookupByCityState(firstArg, secondArg)};					
					break;
				case 'getpopulationbystate':
					firstArg 		= splitArr[1].trim();
					// throw exception if value is falsy.  see previous comment for more info.
					if(!firstArg)
						throw 'errorEmptyString';					
					jsonToClient 	= {"state": firstArg, "pop": cities.getPopulationByState(firstArg)};
					break;
			}

			if(typeof jsonToClient === "undefined")
				throw 'undefinedData';
			else {
				// send data to client
				socket.write(JSON.stringify(jsonToClient).cyan);
			}

		} catch (e) {
			return socket.write("Invalid Request".cyan);
		}
		
	});	// marks the end of socket.on method

});	// marks the end of createServer method

// listen for client connections
server.listen(3000, () => {
	console.log("Listening for connections on port 3000");
});
