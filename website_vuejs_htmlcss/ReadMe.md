﻿## Term Project

Author:	Karn Kausal

Class:		MET CS 601

Date:		12/9/2021

Web Site:  [https://cs601-term-project-kausal.netlify.app](https://cs601-term-project-kausal.netlify.app/)


## Description

A website that showcases Karn's portfolio including the following web pages and information:

- Biography

- Hobbies/Interests

- Web-based contact form

- Includes an interactive photo gallery for famous movie quotes

- Interactive Career Page

## Assumptions

Following assumptions were made during the design and development phase:

- It was assumed acceptable to reuse minor portions of the web page in Assignment 1 and 2 and provide them a significant "face-lift" in the term project.
- The intended audience could be a prospective employer or someone in the tech community who wants to view Karn's portfolio to know more about his technical skills as well as personal interests.
- When the information on the Contact page is submitted, there is no server side php code to handle the submission.  Therefore, browsers such as Firefox will display a blank page when Submit is clicked whereas Chrome or Edge will simply clear all the form entries and remain on the Contact page.
- Legacy Internet Explorer that is being phased out by Microsoft and replaced with Chromium based Edge was not considered a test browser candidate.  The site is optimized for Chrome, Firefox, Edge and Safari.

## Project Details:

> ### Design

-  A  visually pleasing design and exceptional aesthetics was one of the main goals.  To achieve this goal, the following was implemented for the desktop version of the web site:
	- A pleasing "zen" background image was picked after a lot of research.  
	- The main "container" windows has a white background and within this container, there are HTML5 elements such as header, sections and articles.  It should be noted that this white background square window was beautifully placed in the **center** of the page.  Also, this window aligns with the soothing lines of the background image.  As an example, the background image has an elegant wall with pleasing tiles and a "zen-like" floor. The lines on this floor align with the edges of the center window.
> ### Layout
- The layout is liquid and allows the content to adjust as the browser window or viewing device changes.  Media queries were used to ensure that the liquid layout.  As an example, when viewing on smartphones, the device is small and so to keep the design pleasing, the background image was replaced with a simple white background and the nav bar is rearranged.  For tablet, the background image is shown but the viewer is allowed to vertically scroll the content to ensure a smooth experience.  And of course, the desktop version is just as liquid and visually pleasing.

> ### Text
- All text on the site was chosen with readability in mind and is presented with appropriate font-sizes as well as font-family.  The following font-families were chosen because they are considered most common and ensure the most compatibility:

    *font-family: Georgia, Times, serif;*
- All text was thoroughly reviewed to avoid major mechanical errors such as grammar, punctuation, and spelling.


> ### Navigation
- The top navigation bar was carefully designed to ensure it is well organized and easy to navigate.  
- Every page has the same consistent navigation bar so that the relationship between pages is clear and it is easy to find other pages on the site.  In other words, every page is clearly linked to other pages with well-defined labels.
-  The labels for the links clearly inform the user of the intended purpose and what to expect before they click.
- All page titles are specific to the content of the page.  A minimalist approach was taken here as well and therefore each page has a single topic as its focus instead of multitude of topics.
- Lastly, the navigation bar is very clean, minimalist, appealing and very user friendly.  The background color of the navigation bar was chosen carefully to complement the overall aesthetics of the site.


> ### Images
- An external paint program/editor was used to resize images.  This was done to ensure that if a web page contained an image, then the other images also on that specific page are the same size.  As an example, all images on the "Quotes" page are the same size.  All images on the Hobbies page are the same size etc.
- The images add to the overall design and are not distorted.  This is especially evident in the opening animation for the index.html page.
- No negative graphic effects are evident.
- Accessibility:  All images include Alt descriptions to ensure it can be understood by **screen reader software.**

> ### Mechanics
The site is functional and there are no broken links or missing graphics.  The site was tested and validated on Firefox, Chrome, Edge and Safari.  Note:  As mentioned in the Assumptions section, no server side php code was implemented for the Contact form page.  Therefore, browsers such as Firefox will display a blank page when Submit is clicked whereas Chrome or Edge might clear all the form entries or might keep the populated entries as is and remain on the Contact page.

> ### HTML
Mastery of CSS concepts can be observed as follows throughout the site:
- HTML semantics were properly used throughout the code.  All pages on the site use html tags such as header, nav, footer or article to ensure the html tag used is named for what it does.  
- Use of generic element such as div was kept to a minimum and was only used in a couple of places to manage the Vue code.  Since the lecture notes for Vue used div, it was assumed this was an acceptable approach.  Even though span can be used when no other tag seems feasible, even the use of span was kept minimal to manage Vue code.
- 

> ### CSS
Mastery of CSS concepts can be observed as follows throughout the site:

- **External CSS files** were used throughout the development of the site. 
- All pages are styled with a common CSS file titled styles.css.  However, to keep the index.html intro page clean and minimalistic to emphasize the animation, a main.css file was developed.  Overall, other pages were styled using their own unique css file.  This modularization of the code kept it maintainable and easy to manage.
- **CSS Grid** was used to layout the various "grid items" on each of the pages.  This maintained a *consistent* layout to ensure the layout and code is easy to maintain for future updates of the website.  However, instead of the entire body being a grid, only specific HTML5 "sections" utilized grid in unique ways.
	- The Quotes page (that also uses Vue in a unique way) makes use of CSS grid to precisely and beautifully display the images.
- Even though **animations** were not taught in the course, MDN and online resources were used for self-study to create a beautiful animation intro for index.html.  
	- In addition to index.html, the home.html page that appears immediately after index.html makes creative use of CSS animation techniques to "slide" in the text from the top as well as from the right side of the screen.
	-  The **Box Model** that wraps around every html element was carefully managed by making use of various CSS properties such as font-family, color, height, margin, padding and many others.  
		- To show mastery of concepts, some of these properties were also coded with shorthand notation.
		- In addition to using only px, the mastery of concepts was further exemplified by making use of px, percentages (%) as well as rem and em units.  *Note:  In the real world, all measurements would have been kept the same for easily maintaining the code however for an academic course, it was important to display subject matter expertise and hence a variety of measurements were used.  Of course, preference was given to units that are better suited for ***responsive design*** and hence rem and % were utilized in certain areas to better manage browser size changes via media queries.*
- Use of display block vs inline was used appropriately.  As an example, navigation bar was managed using the inline value.
- **Inheritance** concepts were used for those pages that use the global styles.css as well as their own unique CSS files.  For example, pages such as Contact inherit a lot of the styling from the styles.css.  However, the Contact page then also overrides and builds by using its own file titled stylescontact.css.
- Selectors were used in combination with **specificity**.  This was in addition to making use of ID and Class selectors to identify the part of the DOM that required styling.
- Positioning of the background image as well as header were carefully implemented using fixed vs relative.
- **Media queries** were used in the CSS files to ensure a responsive design when viewing on smaller screens such as smartphones, tablets or desktops.
	- Online research showed that the width for elements such as the drop down on the Contact page differ when viewed on Chrome vs Firefox.  Therefore, media queries were also used to adjust the width of the drop down based on the type of browser being used by the user.  This ensured the width of the drop down appeared correctly on different browsers.
- The table on the Hobbies page was also beautifully styled via CSS.


> ### JavaScript
Mastery of JavaScript concepts can be observed as follows throughout the site:

- **External JS files** were used throughout the development of the site.
- **Block scoped** statements such as let and const were used for better flexibility.
- The Career page is a great example where instead of providing a plain text resume, JS is used to provide **interactivity**.  This page makes use of the `style.display` value of a DOM element to manage the interactions provided via the radio buttons implemented via HTML.  It should be noted that the style.display was not taught in the course and thus the knowledge was acquired via self-teaching of these JS concepts.
- Events are handled via the **addEventListener**.
- Code is maintained via the use of **functions** through the site.
- To exhibit mastery of **conditionals** and **loops**, the for...of loop is used in combination with the Vue code on the Contact page that makes use of conditional statements as well as the **negation** operator.
- Descriptive names and camelCasing was used to combine words and keep functions focused on a single task.
- As mentioned earlier, the home.html page has a beautiful animation **where the text slides from the top** as well as the right side.  This was also a self-taught concept and it required the use of style.animation as well as style.visibility values to correctly manage the timing with JavaScript code.
- **JSON and Fetch:**  The Fetch API was used to retrieve JSON data from a server.  This data was used for the interactive Career page and used to display the degree information such as type of degree, school and the major. However, the code implementation is not identical to the homework assignment.  The code was given a facelift to individually store the degree information in different variables and then this data was traversed using a for loop.

> ### Vue
Mastery of JavaScript concepts can be observed as follows throughout the site:

- Instead of building a form purely with HTML, the form on the Contact page was designed with Vue.js.
- The Vue instance was not embedded as part of the script tag.  Instead, for better modularity and clean code, the instance was implemented in its own vuecontact.js file.
- The Mastery of Vue really shines on the interactive Quotes page.  The page makes use of vue.js file and includes the following:
	- **expressions** to properly format and display the welcome message as well as the note.
	- The v-on **directive** is applied for reactive behavior and data binding.  In addition to mouseover and mouseleave, the `v-on:click` was also implemented to ensure that those on touch screen devices can click the images.  This was done for obvious reasons as mouse hover functionality is not available on touch devices.
	- **Attribute binding** was used to bind Vue data properties to html elements.
	- **Complex conditionals** such as `v-else-if-else` were used to make the Quotes page very interactive.
	- As mentioned before, the v-on directive was used and therefore **events** were handled via this v-on directive.  To express subject matter expertise, a variety of events such as click, mouseover and mouseleave were used.
- The Contact Page also made use of Vue in a creative sense.  In addition to using HTML5 validation for the email field, the validation for the first name and last name was done using Vue to ensure the user cannot leave them blank.
	- Vue was used to display a variety of different error message on the screen.  For example, if the last name was left blank, an error message displayed in red text.  If first name was left blank, a similar red text message displayed.  And if both were blank, all errors were displayed in a clean and neat format below the Submit button.
	- It should be noted that **arrays** were used in the Vue data property to easily store and maintain a list of error messages.  To keep the code clean and simple, the **`for..of`** loop was used to traverse the list of error message.  
> ### DOM
Mastery of DOM concepts can be observed as follows throughout the site:

- Throughout the site, the DOM element are being manipulated.  For example, getElementById() has been to manipulate various elements in the JavaScript files.
- The code also makes use of window.onload method to control behavior such as animation on the home.html page.
- As mentioned before, pages such as the Career page do some complex DOM manipulation to provide an interactive resume via the use of the radio buttons.
- In summary, the DOM manipulation is used heavily and can be observed throughout the code.  However, for brevity, not every occurence is mentioned here.

> ### Validation/Accessibility
Mastery of Validation/Accessiblity concepts can be observed as follows throughout the site:
- **Responsive design:**  Media queries have been used extensively throughout the site to ensure a smooth experience on small screens such as phones and medium devices such as tablets.  Since this course was not about mobile web development, the default experience on desktop comes with all the "bells and whistles."  
- Nevertheless, extensive testing was done to ensure a smooth experience on touch devices as well as lower resolutions such 1280x720.  Resolutions below 720p were not considered and tested because the most common lowest resolution on TV or monitor in 2021 seems to be 720p.  It can be argued based on online research that FHD 1080p monitors are the default these days however the site was still tested for 720p resolution.
- **Alt text** was used on all images to ensure compatibility with **screen readers**.
* The CSS code was validated on the following website:  https://jigsaw.w3.org/css-validator/

  - Upon validation, the following message was received for all CSS files:  "Congratulations! No Error Found."
  
* The HTML5 code was validated at the following website:  https://validator.w3.org/
 All HTML code validated without errors.  As discussed in class, there were some exceptions as noted below:
	* For most html files, generic warnings (similar to those encountered during homework assignments) were encountered:
		* *Article lacks heading. Consider using `h2`-`h6` elements* 
 
	* Errors:  Since the Contact page form does not implement any action for posting to server, the following error was received as expected:
		* *Bad value for attribute `action` on element `form` Must be non-empty.*
	* As discussed in class, the html files that implement Vue generated a lot errors.  However, per Professor's guidance, it was assumed that these can be ignored.  A lot of errors were generated for the "Attribute v-on" not allowed when a lot of these statements are perfectly valid.  As an example, the following error was generated:
		* *Attribute `v-on:submit.prevent` not allowed on element `form`  at this point.*  However, stack overflow and online research shows this is perfectly valid as shown in the code at the following links:  
		https://www.better.dev/processing-a-form-with-vue
		https://www.w3resource.com/vue/event-handling.php
* The site was validated and tested on:
	* Desktop/Laptop computer with FHD 1080p resolution (Recommended).
	* iPad Pro (2nd Generation).
	* iPad Mini (2nd Generation).
		* On the iPad, media queries were written to optimize both Portrait and Landscape mode.  In the code, the only the Portrait mode is explicitly specified.  The default code is for landscape mode.
	* iPhone 13 Pro Max.
	
> ### Extra Credit
- CSS Grid was carefully studied and has been implemented throughout the site.  It should be noted that even pages with Vue, forms, mouseover and animations still have the CSS Grid and **combining all these technologies with CSS Grid was not trivial.**  In summary, it was extensively time consuming given that CSS Grid was not taught in the course and every concept had to be learned outside the course via self-teaching. 
