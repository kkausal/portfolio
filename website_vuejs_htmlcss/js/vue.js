let app = new Vue({
    el: '#app',
    data: {
        welcomemsg: "Karn loves famous quotes. Interact with above images and have fun. "                   +
                    "For desktop users, hovering your mouse over an image will show an alternate image & "  +
                    "a quote will appear in this section."                                                  +
                    "  If using a touch screen device, click/tap the image instead and have fun.",
        note:       "Note:  Not all interactive features available on touch screen devcies.  As an example, on desktop, the alternate image reverts " +
                    "back to the original image when your mouse moves away from the image.  Not all mobile browsers will revert to the alternate image after click/tap.",
        logoBatman: './images/quotes/batmanlogoresized.jpg',
        logoCatwoman: './images/quotes/catwoman.jpg',
        logoMario: './images/quotes/mario.jpg',
        logoPhone: './images/quotes/apple.png',

        isItJokerBatman: false,
        isItCatBruce: false,
        isItMarioBowser: false,
        isItAppleAndroid: false
    },

    methods: {
        // switch to alternate image (joker) when mouse hovers  
        mouseOverJoker: function() {
            this.logoBatman = './images/quotes/joker.jpg';
            this.isItJokerBatman = true;
        },
        // switch back to previous image (batman) when mouse leaves 
        mouseLeaveBatman: function() {
            this.logoBatman = './images/quotes/batmanlogoresized.jpg';
            this.isItJokerBatman = false;
        },

        // switch to alternate image (real life cat & bruce) when mouse hovers  
        mouseOverCatwoman: function() {
            this.logoCatwoman = './images/quotes/catwoman_bruce_resized.jpg';
            this.isItCatBruce = true;
        },
        // switch back to previous image (drawing of cat & bruce) when mouse leaves 
        mouseLeaveCatBruce: function() {
            this.logoCatwoman = './images/quotes/catwoman.jpg';
            this.isItCatBruce = false;
        },

        // switch to alternate image (bowser) when mouse hovers          
        mouseOverBowser: function() {
            this.logoMario = './images/quotes/bowser.jpg';
            this.isItMarioBowser = true;
        },
        // switch back to previous image (mario) when mouse leaves 
        mouseLeaveMario: function() {
            this.logoMario = './images/quotes/mario.jpg';
            this.isItMarioBowser = false;
        },

        // switch to alternate image (android) when mouse hovers  
        mouseOverAndroid: function() {
            this.logoPhone = './images/quotes/android.jpg';
            this.isItAppleAndroid = true;
        },
        // switch back to previous image (apple) when mouse leaves 
        mouseLeaveApple: function() {
            this.logoPhone = './images/quotes/apple.png';
            this.isItAppleAndroid = false;
        }

    }

});