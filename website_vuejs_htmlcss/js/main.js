let el_welcomeMsg = document.getElementById("welcomemsg");
let el_msgPara1 = document.getElementById("mainpara1");

// animate the 
function displayText() {
    showWelcomeMsg();
    showMsgPara1();    
}

function showWelcomeMsg(){
    el_welcomeMsg.style.visibility = "visible";    
    el_welcomeMsg.style.animation = "slidedown 4s 1";   
}

function showMsgPara1(){    
    el_msgPara1.style.visibility = "visible";
    el_msgPara1.style.animation = "slideleft 4s 1";   

}


window.onload = displayText;
