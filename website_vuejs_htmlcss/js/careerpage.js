// constants to use in the template strings for displaying JSON data fetched from server
const UW        = 0;
const BOSTON    = 1;

// declaration for DOM manipulation
let btnCompanies                = document.getElementById("companies");
let btnExperience               = document.getElementById("experience");
let btnDegrees                  = document.getElementById("degrees");
let el_articleMessageExperience = document.getElementById("articleMessageExperience");
let msgAreaCompanies            = document.getElementById("articleMessageCompanies");
let msgAreaDegrees              = document.getElementById("articleMessageDegrees");
let msgAreaExperience           = document.getElementById("msgAreaExperience");

let msgAreaUW                   = document.getElementById("uw");
let msgAreaBoston               = document.getElementById("boston");

// arrays to store the JSON data fetched from the server.
let school  = [];
let major   = [];
let type    = [];

async function getDegrees() {

    let newContent          = "";
    let degreesRetrieved    = [];
    let url                 = "/data/my_degrees.json";

    // The fetch() method returns a Promise so you can use the then() to handle it
    // When the request completes, the resource is available. At this time, the promise will resolve into a "response" object.
    await fetch(url)
        //get your data here, and check for the response status.         
        .then(
            (response) => {
                // now that the promise has resolved into a response object, use the response object to check the status
                if (response.status === 200) {
                    newContent += `<p> Server responded with a Status Code of: ${response.status} </p>`;
                    return response.json();
                }
            })
        // the response further resolves into a data object that is used to parse the degree 
        // info stored in the json that was fetched earlier from the server.
        .then((data) => {
            for (let i = 0; i < data.my_degrees.length; i++) {
                type[i] = data.my_degrees[i].data.type;
                major[i] = data.my_degrees[i].data.major; 
                school[i] = data.my_degrees[i].data.school;                
            }
        });

        msgAreaUW.innerHTML = `Karn attended the ${school[UW]} and graduated with a ${type[UW]} in ${major[UW]}.  His area of concentration within EE was embedded systems.`;
        msgAreaBoston.innerHTML = ` Karn is presently attending the awesome ${school[BOSTON]}.
        After spending over a decade in hardware engineering & design, Karn
        returned back to school in 2020 and is pursuing his ${type[BOSTON]} in ${major[BOSTON]}.`;
}

// Following code manipulates DOM based on radio button selections

// initialize the styles and height
msgAreaCompanies.style.display              = "none";
msgAreaExperience.style.display             = "none";
msgAreaDegrees.style.display                = "none";
el_articleMessageExperience.style.height    = "25rem";


// Display company info but not experience or degree info
function displayCompanies(){
    msgAreaCompanies.style.display          = "block";
    msgAreaExperience.style.display         = "none";
    msgAreaDegrees.style.display            = "none";
    el_articleMessageExperience.style.height = "2rem";
}

// Display experience info but not company or degree info
function displayExperience(){
    msgAreaCompanies.style.display          = "none";
    msgAreaDegrees.style.display            = "none";
    msgAreaExperience.style.display         = "block";
    el_articleMessageExperience.style.height = "max-content";

}

// Display degree info but not experience or company info
function displayDegrees(){
    // first fetch the JSON degree data from the server
    getDegrees();
    // after fetch completes, manipulate the DOM elements
    msgAreaCompanies.style.display          = "none";
    msgAreaExperience.style.display         = "none";
    msgAreaDegrees.style.display            = "block";
    el_articleMessageExperience.style.height = "max-content";

}


// perform event handling and display the appropriate radio button event/selection
btnCompanies.addEventListener("click", displayCompanies, false);
btnExperience.addEventListener("click", displayExperience, false);
btnDegrees.addEventListener("click", displayDegrees, false);