let app = new Vue({
    el: '#app',
    data: {
        errorList: [],
        fname: null,
        em: null,
        lname: null,
        selectedOption: true
    },

    methods: {

        // This method uses two techniques to access the html input text field (first name & last name).
        // First method uses DOM method getElementById and the other uses Vue's ref ID.
        validateForm: function(e) {
            // at the start of validation, if user has entered both first and last name, 
            // there is no need to check for errors and hence return.
            if(this.fname && this.lname){
                // clear out the error list at the start of each validation
                this.errorList = [];
                return true;
            }

            // clear out the error list at the start of each validation
            this.errorList = [];
            // retrieve the DOM element for last name
            let el_lastname = document.getElementById("txtlastName");
            if(!this.fname) {
                this.errorList.push("First name is required.");
                // use Vue's ref ID to access child components
                // reference:  https://vuejs.org/v2/guide/components-edge-cases.html#Accessing-Child-Component-Instances-amp-Child-Elements
                this.$refs.first.focus();
            }
            if(!this.lname) {
                this.errorList.push("Last name is required.");
                el_lastname.focus();
            }


            e.preventDefault();

        } // end function validateForm

    }

});