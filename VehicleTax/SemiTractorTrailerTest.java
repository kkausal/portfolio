import org.junit.Assert;
// import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;
import java.text.DecimalFormat;

   /**
  * A JUnit test file to test the methods in the
  * Semi Tractor Trailer class.
  */

public class SemiTractorTrailerTest {


   static final double TAX_RATE = 0.02, 
                        PER_AXLE_TAX_RATE = 0.005,
                        LARGE_TRUCK_TAX_RATE = 0.03;

   /** Fixture initialization (common initialization
    *  for all tests). **/
   @Before public void setUp() {
   }

   /** Test getAxles method.  
    * @throws NegativeValueException from scanning negative input data.
    */
   @Test public void getAxlesTest() throws NegativeValueException {
      
      SemiTractorTrailer semi1 = 
            new SemiTractorTrailer("Williams, Pat", 
               "2012 International Big Boy", 45000, false, 5.0, 4);
         
      Assert.assertEquals("getAxles Passed", 4, semi1.getAxles());
   }
   
   /** Test setAxles method.  
    * @throws NegativeValueException from scanning negative input data.
    */
   @Test public void setAxlesTest() throws NegativeValueException {
      
      SemiTractorTrailer semi1 = 
            new SemiTractorTrailer("Williams, Pat", 
               "2012 International Big Boy", 45000, false, 5.0, 4);
      
      semi1.setAxles(6);
                              
      Assert.assertEquals("setAxles Passed", 6, semi1.getAxles());
   }  
   
      /** Test toString method.  
    * @throws NegativeValueException from scanning negative input data.
    */
     
   @Test public void toStringTest() throws NegativeValueException {
      
      String output = "";
      DecimalFormat df = new DecimalFormat("$#,##0.00");
      
   
      SemiTractorTrailer semi1 = 
            new SemiTractorTrailer("Williams, Pat", 
               "2012 International Big Boy", 45000, false, 5.0, 4);            
      
      output = semi1.getOwner() + ": " + semi1.getClass().getName() 
             + " " + semi1.yearMakeModel;
         
      output += "\n" + "Value: " + df.format(semi1.getValue()) 
             + " Use Tax: " + df.format(semi1.useTax());
      
      output += "\n" + "with Tax Rate: " + TAX_RATE;
      output += " Large Truck Tax Rate: " + LARGE_TRUCK_TAX_RATE;
      output += " Axle Tax Rate: " + PER_AXLE_TAX_RATE * semi1.getAxles();
      
   
      Assert.assertEquals("toString Passed", output, semi1.toString());
                                 
      
   } // end method toString. 

   /** Test useTax method.  
    * @throws NegativeValueException from scanning negative input data.
    */
   @Test public void useTaxTest() throws NegativeValueException {
   
      SemiTractorTrailer semi1 = 
            new SemiTractorTrailer("Williams, Pat", 
               "2012 International Big Boy", 45000, false, 5.0, 4);            
      
      Assert.assertEquals("useTax test", 3150.0, semi1.useTax(), 0.000001);
   
   }            
   
}
