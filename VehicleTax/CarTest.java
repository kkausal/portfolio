import org.junit.Assert;
//import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;
import java.text.DecimalFormat;


/**
 * Junit test file to test the methods in Vehicle and Car class.
 */

public class CarTest {

   static final double TAX_RATE = 0.01, 
                              ALTERNATIVE_FUEL_TAX_RATE = 0.005, 
                              LUXURY_THRESHOLD = 50_000, 
                              LUXURY_TAX_RATE = 0.02; 


   /** Fixture initialization (common initialization
    *  for all tests). **/
   @Before public void setUp() {
   }


   /** Test getOwner method in Vehicle. 
    * @throws NegativeValueException from scanning negative input data.
    */
   @Test public void getOwnerTest() throws NegativeValueException {
      
      Car car1 = new Car("Jones, Sam", "2017 Honda Accord", 22000, false);
                  
      Assert.assertEquals("getOwner Passed", "Jones, Sam", car1.getOwner());
   }
   
   /** Test setOwner method in Vehicle. 
    * @throws NegativeValueException from scanning negative input data.
    */
   @Test public void setOwnerTest() throws NegativeValueException {
      
      Car car1 = new Car("Jones, Sam", "2017 Honda Accord", 22000, false);
      car1.setOwner("Wayne, Bruce");
                  
      Assert.assertEquals("setOwner Passed", "Wayne, Bruce", car1.getOwner());
   }


   /** Test getYearMakeModel method in Vehicle. 
    * @throws NegativeValueException from scanning negative input data.
    */
   @Test public void getYearMakeModelTest() throws NegativeValueException {
      
      Car car1 = new Car("Jones, Sam", "2017 Honda Accord", 22000, false);
                  
      Assert.assertEquals("getYearMakeModel Passed", "2017 Honda Accord", 
         car1.getYearMakeModel());
   }      

   /** Test setYearMakeModel method in Vehicle. 
    * @throws NegativeValueException from scanning negative input data.
    */
   @Test public void setYearMakeModelTest() throws NegativeValueException {
      
      Car car1 = new Car("Jones, Sam", "2017 Honda Accord", 22000, false);
      car1.setYearMakeModel("1967 Ford Mustang");
                        
      Assert.assertEquals("setYearMakeModel Passed", "1967 Ford Mustang", 
         car1.getYearMakeModel());
   }  
   
   /** Test getValue method in Vehicle.  
    * @throws NegativeValueException from scanning negative input data.
    */
   @Test public void getValueTest() throws NegativeValueException {
      
      Car car1 = new Car("Jones, Sam", "2017 Honda Accord", 22000, false);
                  
      Assert.assertEquals("getValue Passed", 22000, car1.getValue(), 0.000001);
   }
   
   /** Test setValue method in Vehicle.  
    * @throws NegativeValueException from scanning negative input data.
    */
   @Test public void setValueTest() throws NegativeValueException {
      
      Car car1 = new Car("Jones, Sam", "2017 Honda Accord", 22000, false);
      car1.setValue(90000);
                  
      Assert.assertEquals("setValue Passed", 90000, car1.getValue(), 0.000001);
   } 

   /** Test getAlternativeFuel method in Vehicle.  
    * @throws NegativeValueException from scanning negative input data.
    */
   @Test public void getAlternativeFuelTest() throws NegativeValueException {
      
      Car car2 = 
         new Car("Jones, Jo", "2017 Honda Accord", 22000, true);
      Assert.assertTrue("getAlternativeFuel True", car2.getAlternativeFuel());
   }
   
   /** Test setAlternativeFuel method in Vehicle.  
    * @throws NegativeValueException from scanning negative input data.
    */
   @Test public void setAlternativeFuelTest() throws NegativeValueException {
      
      Car car2 = new Car("Jones, Jo", "2017 Honda Accord", 22000, true);
      car2.setAlternativeFuel(false);
                  
      Assert.assertFalse("setAlternativeFuel False", car2.getAlternativeFuel());
   }   
   
   /** Test getVehicleCount method in Vehicle.  
    * @throws NegativeValueException from scanning negative input data.
    */
   @Test public void getVehicleCount() throws NegativeValueException {
      
      Vehicle.resetVehicleCount();            
      Car car1 = new Car("Jones, Sam", "2017 Honda Accord", 22000, false);
      Assert.assertEquals("getVehicleCount Passed", 1, 
         Vehicle.getVehicleCount());
   }
   
   /** Test resetVehicleCount method in Vehicle.  
    * @throws NegativeValueException from scanning negative input data.
    */
   @Test public void resetVehicleCount() throws NegativeValueException {
      
      Vehicle.resetVehicleCount();
                  
      Assert.assertEquals("resetVehicleCount Passed", 0, 
         Vehicle.getVehicleCount());
   }    
   
   /** Test toString method in Vehicle & Car.  
    * @throws NegativeValueException from scanning negative input data.
    */
   @Test public void toStringTest() throws NegativeValueException {
   
                     
      String output = "";
      DecimalFormat df = new DecimalFormat("$#,##0.00");
      
   /** Test car with no alternative fuel  
    * @throws NegativeValueException from scanning negative input data.
    */
      Car car1 = new Car("Jones, Sam", "2017 Honda Accord", 22000, false);
      
      
      output = car1.getOwner() + ": " + car1.getClass().getName() 
             + " " + car1.yearMakeModel;
         
      output += "\n" + "Value: " + df.format(car1.getValue()) 
             + " Use Tax: " + df.format(car1.useTax());
      
      output += "\n" + "with Tax Rate: " + TAX_RATE;
                  
      Assert.assertEquals("toString Passed", output, car1.toString());
   
                  
   
   }    
   
   /** Test toString method in Vehicle & Car.  
    * @throws NegativeValueException from scanning negative input data.
    */
   @Test public void toStringAltFuelTest() throws NegativeValueException {
   
      String outputAltFuel = "";
      DecimalFormat df = new DecimalFormat("$#,##0.00");
         
   
   /** Test car with alternative fuel & luxury threshold  
    * @throws NegativeValueException from scanning negative input data.
    */
      Car car4 = 
         new Car("Smith, Jack", "2015 Mercedes-Benz Coupe", 110000, true);
      
      outputAltFuel = car4.getOwner() + ": " + car4.getClass().getName() 
             + " " + car4.yearMakeModel + " (Alternative Fuel)";
         
      outputAltFuel += "\n" + "Value: " + df.format(car4.getValue()) 
             + " Use Tax: " + df.format(car4.useTax());
      
      outputAltFuel += "\n" + "with Tax Rate: " + ALTERNATIVE_FUEL_TAX_RATE;
   
      outputAltFuel += " Luxury Tax Rate: " + LUXURY_TAX_RATE;            
                             
   
                  
      Assert.assertEquals("toString Passed", outputAltFuel, car4.toString());
      
   }

   /** Test equals method in Vehicle when both cars are equal.  
    * @throws NegativeValueException from scanning negative input data.
    */
   @Test public void equalsTrueTest() throws NegativeValueException {
   
      Car car1 = new Car("Jones, Sam", "2017 Honda Accord", 22000, false);
      Car car1Duplicate = 
         new Car("Jones, Sam", "2017 Honda Accord", 22000, false);
      Car car4 = 
         new Car("Smith, Jack", "2015 Mercedes-Benz Coupe", 110000, true);
               
      Assert.assertTrue("cars equal passed", car1.equals(car1Duplicate));      
   } 
   
   /** Test equals method in Vehicle when both cars are not equal.  
    * @throws NegativeValueException from scanning negative input data.
    */
   @Test public void equalsFailTest() throws NegativeValueException {
   
      Car car1 = new Car("Jones, Sam", "2017 Honda Accord", 22000, false);
      Car car4 = new Car("", "2015 Mercedes-Benz Coupe", 110000, true);         
   
      Assert.assertFalse("cars not equal passed", car1.equals(car4));         
   
   }
   
   /** Test equals method in Vehicle when obj is not an instance of car.  
    * @throws NegativeValueException from scanning negative input data.
    */
   @Test public void equalsInstanceTest() throws NegativeValueException {
   
      Car car1 = new Car("Jones, Sam", "2017 Honda Accord", 22000, false);
      String s = "";
               
      Assert.assertFalse("cars not equal passed", car1.equals(s));         
   
   }
   
   /** Test hashCode method.  
    * @throws NegativeValueException from scanning negative input data.
    */
   @Test public void hashCodeTest() throws NegativeValueException {
   
      Car car1 = new Car("Jones, Sam", "2017 Honda Accord", 22000, false);
      Car car4 = new Car("", "2015 Mercedes-Benz Coupe", 110000, true);         
               
      Assert.assertTrue("hashCode test", car1.hashCode() == car4.hashCode());
   
   } 
   
   /** Test useTax method.  
    * @throws NegativeValueException from scanning negative input data.
    */
   @Test public void useTaxTest() throws NegativeValueException {
   
      Car car1 = new Car("Jones, Sam", "2017 Honda Accord", 22000, false);
               
      Assert.assertEquals("useTax test", 220.00, car1.useTax(), 0.000001);
   
   }   
   
                                                   
}
