import java.util.Comparator;

/**
 * A UseTaxComparator class implements the Comparator 
 * interface for Vehicle objects.
 * 
 * Project 5
 *
 * @author Karn Kausal - CPSC 1223 - A01
 * @version 09192019
 */
public class UseTaxComparator implements Comparator<Vehicle> {
   /**
    * Defines the ordering from lowest to highest based on the 
    * use tax for v1 and v2.
    * @param v1 represents the first vehicle.
    * @param v2 represents the second vehicle.
    * @return int where -1 represents v1 < v2, 0 is v1 = v2 and
    * 1 is v1 > v2.
    */
   public int compare(Vehicle v1, Vehicle v2) {
   
      if (v1.useTax() < v2.useTax()) {
      
         return -1;
      
      }
      else if (v1.useTax() == v2.useTax()) {
      
         return 0;
         
      }
      else {
         return 1;
      }
   
   
   }  // end method compare   
   
} // end class