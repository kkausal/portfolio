import org.junit.Assert;
//import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

 /**
  * A JUnit test file to test the methods in the NegativeValueException class.
  */   
public class NegativeValueExceptionTest {


   /** Fixture initialization (common initialization
    *  for all tests). **/
   @Before public void setUp() {
   }

    /**
     * Test when "value" is negative.
     */
   @Test public void negativeValueExceptionTest() { 
      boolean thrown = false;
         
      try {
         Car car = new Car("Jackson, Bo", "2012 Toyota Camry", -25000, false); 
      }
         
      catch (NegativeValueException e) { 
         
         thrown = true;
      }
         
      Assert.assertTrue("Expected NegativeValueException to be thrown.",
            thrown); 
   }

    /**
     * Test when "tons" is negative.
     */      
   @Test public void negativeTonsExceptionTest() { 
      boolean thrown = false;
         
      try {
      // set tons to a negative value
         Truck truck1 = 
            new Truck("Williams, Jo", "2012 Chevy Silverado", 30000, 
               false, -1.5); 
            // set value to a negative value                  
         Truck truck2 = 
            new Truck("Williams, Jo", "2012 Chevy Silverado", -30000, 
               false, 1.5);                  
      }
         
      catch (NegativeValueException e) { 
         
         thrown = true;
      }
         
      Assert.assertTrue("Expected NegativeValueException to be thrown.",
            thrown); 
   }     
   
    /**
     * Test truck when "value" is negative but tons is positive.
     */      
   @Test public void negativeTruckValueExceptionTest() { 
      boolean thrown = false;
         
      try { 
            // set truck value to a negative value                  
         Truck truck1 = 
            new Truck("Williams, Jo", "2012 Chevy Silverado", -30000, 
               false, 1.5);                  
      }
         
      catch (NegativeValueException e) { 
         
         thrown = true;
      }
         
      Assert.assertTrue("Expected NegativeValueException to be thrown.",
            thrown); 
   }     
   

    /**
     * Test when "axles" is negative.
     */      
   @Test public void negativeAxlesExceptionTest() { 
      boolean thrown = false;
         
      try {
      
         SemiTractorTrailer semi1 = 
            new SemiTractorTrailer("Williams, Pat", 
               "2012 International Big Boy", 45000, false, 5.0, -4); 
      }
         
      catch (NegativeValueException e) { 
         
         thrown = true;
      }
         
      Assert.assertTrue("Expected NegativeValueException to be thrown.",
            thrown); 
   }   

    /**
     * Test when "axles" is positive but tons is negative.
     */      
   @Test public void negativeAxles2ExceptionTest() { 
      boolean thrown = false;
         
      try {
      
         SemiTractorTrailer semi1 = 
            new SemiTractorTrailer("Williams, Pat", 
               "2012 International Big Boy", 45000, false, -5.0, 4); 
      }
         
      catch (NegativeValueException e) { 
         
         thrown = true;
      }
         
      Assert.assertTrue("Expected NegativeValueException to be thrown.",
            thrown); 
   }   
   
    /**
     * Test when "engineSize" is negative.
     */      
   @Test public void negativeEngineExceptionTest() { 
      boolean thrown = false;
         
      try {        
      
         Motorcycle bike1 = new Motorcycle("Brando, Marlon",
            "1964 Harley-Davidson Sportster", 14000, false, -750);
      
      }
         
      catch (NegativeValueException e) { 
         
         thrown = true;
      }
         
      Assert.assertTrue("Expected NegativeValueException to be thrown.",
            thrown); 
   }            
}
