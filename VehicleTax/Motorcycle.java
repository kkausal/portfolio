/**
  * A Motorcyle class dervied from Vehicle.
  * 
  * Project 4
  *
  * @author Karn Kausal - CPSC 1223 - A01
  * @version 09122019
  */
public class Motorcycle extends Vehicle {

   //------------------------constants-------------------------------//
   /** 
    * Public constants.
    */
   public static final double TAX_RATE = 0.005, 
                              ALTERNATIVE_FUEL_TAX_RATE = 0.0025, 
                              LARGE_BIKE_CC_THRESHOLD = 499, 
                              LARGE_BIKE_TAX_RATE = 0.015;
                              
   //------------------------instance variables----------------------//
 
   protected double engineSize;

   //------------------------constructors----------------------------//
   /**
    * Constructor to set the values for the Motorcycle.
    * @param ownerIn represents the vehicle's owner.
    * @param yearMakeModelIn represents the vehicle's year, make and model.
    * @param valueIn represents value of the vehicle.
    * @param altFuelIn represents alternative fuel.
    * @param engineSizeIn represents the engine size.
    * @throws NegativeValueException from scanning negative input data.        
    */
   public Motorcycle(String ownerIn, String yearMakeModelIn, double valueIn, 
                  boolean altFuelIn, double engineSizeIn) throws 
                     NegativeValueException {
                  
      super(ownerIn, yearMakeModelIn, valueIn, altFuelIn);
   
      // throw exception if negative data is received         
      if (engineSizeIn < 0) {
         
         vehicleCount--;
      
         throw new NegativeValueException();
      
      } 
      
      this.engineSize = engineSizeIn;
              
      
   } // end Constructor
   
   //------------------------methods---------------------------------//
    
   
   /**
    * @return double representing the Motorcycle's engine size.
    */
   public double getEngineSize() {
   
      return this.engineSize;
   
   } // end method getEngineSize
 
   /**
    * @param engineSizeIn represents Motorcycle's engine size.
    */
   public void setEngineSize(double engineSizeIn) {
    
      this.engineSize = engineSizeIn;
    
   } // end method setEngineSize

   /**
    * Implement the abstract method defined in the Vehicle class.
    * @return double representing the total use tax.
    */
   public double useTax() {
      
      double totalUseTax = 0;
      
      // check whether or not motorcycle uses alternative fuel
      if (this.altFuel) {
         
         totalUseTax = this.value * ALTERNATIVE_FUEL_TAX_RATE;
      
      }
      else {
      
         totalUseTax = this.value * TAX_RATE;
      }
      
      /* if engine size exceeds threshold, add threshold rate to total use tax.
       */
      if (this.engineSize > LARGE_BIKE_CC_THRESHOLD) {
      
         totalUseTax += (this.value * LARGE_BIKE_TAX_RATE);
      
      }
      
      return totalUseTax;
   
   } // end method useTax
   
   /**
     * 
     * @return String describing the Motorycle.
     */      
   public String toString() {
      
      String output = "";
         
      output = super.toString() + "\n" + "with Tax Rate: ";
      
      /* if Motorcycle has alternative fuel, print the alt fuel tax rates.
       * Otherwise, print the regular tax rate. 
       */    
      if (this.altFuel) {
            
         output += ALTERNATIVE_FUEL_TAX_RATE; 
                
      }         
      else {
      
         output += TAX_RATE;
      } 
      
      // print the large bike tax rate if engine size exceeds threshold.
      if (this.engineSize > LARGE_BIKE_CC_THRESHOLD) {
      
         output += " Large Bike Tax Rate: " + LARGE_BIKE_TAX_RATE;            
      
      }
                    
         
      return output;
                        
      
   } // end method toString.    
   

} // end class