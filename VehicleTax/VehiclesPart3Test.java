import org.junit.Assert;
//import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

/**
 * A JUnit test file to test the functionality in the VehiclesPart3 class.
 */   
public class VehiclesPart3Test {


   /** Fixture initialization (common initialization
    *  for all tests). **/
   @Before public void setUp() {
   }


   /** Test VehiclesPart3.
    * Good File name test. 
    */
   @Test public void vehiclesPart3GoodFileTest() {
   
      VehiclesPart3 vPart3Obj = new VehiclesPart3();
      String [] args = {"vehicles2.txt"};
      Vehicle.resetVehicleCount();
      
      vPart3Obj.main(args);
      Assert.assertEquals("Vehicle.vehicleCount should be 9.", 9, 
         Vehicle.getVehicleCount());
   }
   
   /** Test VehiclesPart3.
    * Bad File name test.        
    */
   @Test public void vehiclesPart3BadFileTest() {
   
      VehiclesPart3 vPart3Obj = new VehiclesPart3();
      String [] args = {"v.ehicles2.txt"};
      Vehicle.resetVehicleCount();
      
      vPart3Obj.main(args);
      Assert.assertEquals("Vehicle.vehicleCount should be 0.", 0, 
         Vehicle.getVehicleCount());
   }      
   
   /** Test VehiclesPart3.
    * No File name test.        
    */
   @Test public void vehiclesPart3NoFileTest() {
   
      VehiclesPart3 vPart3Obj = new VehiclesPart3();
      String [] args = {""};
      Vehicle.resetVehicleCount();
      
      vPart3Obj.main(args);
      Assert.assertEquals("Vehicle.vehicleCount should be 0.", 0, 
         Vehicle.getVehicleCount());
   }   
   
   /** Test VehiclesPart3.
    * Test args length zero.        
    */
   @Test public void vehiclesPart3NoArgsTest() {
   
      VehiclesPart3 vPart3Obj = new VehiclesPart3();
      String [] args = {};
      Vehicle.resetVehicleCount();
      
      vPart3Obj.main(args);
      Assert.assertEquals("Vehicle.vehicleCount should be 9.", 0, 
         Vehicle.getVehicleCount());
   }           
   
} // end class
