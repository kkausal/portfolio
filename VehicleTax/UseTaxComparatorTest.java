import org.junit.Assert;
//import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

/**
 * A JUnit test file to test the methods in the UseTaxComparator class.
 */     
public class UseTaxComparatorTest {


   /** Fixture initialization (common initialization
    *  for all tests). **/
   @Before public void setUp() {
   }


   /** Test v1 < v2.  
    * @throws NegativeValueException from scanning negative input data.
    */
   @Test public void compareTest() throws NegativeValueException {
   
      Vehicle v1 = new Car("Jones, Jo", "2017 Honda Accord", 22000, true);
      Vehicle v2 = 
         new Motorcycle("Brando, Marlon", "1964 Harley-Davidson Sportster", 
            14000, false, 750);
      UseTaxComparator uC = new UseTaxComparator();
   
      Assert.assertEquals("v1 tax < v2 tax Pass", -1, uC.compare(v1, v2));
   }
   
   /** Test v1 equals v2.  
    * @throws NegativeValueException from scanning negative input data.
    */
   @Test public void compareTest2() throws NegativeValueException {
   
      Vehicle v1 = new Car("Jones, Jo", "2017 Honda Accord", 22000, true);
      Vehicle v2 = new Car("Jones, Jo", "2017 Honda Accord", 22000, true);
      UseTaxComparator uC = new UseTaxComparator();
   
      Assert.assertEquals("v1 tax equals v2 tax Pass", 0, uC.compare(v1, v2));
   }    
   
   /** Test v1 > v2.  
    * @throws NegativeValueException from scanning negative input data.
    */
   @Test public void compareTest3() throws NegativeValueException {
   
      Vehicle v1 = 
         new Motorcycle("Brando, Marlon", "1964 Harley-Davidson Sportster", 
            14000, false, 750);
      Vehicle v2 = new Car("Jones, Jo", "2017 Honda Accord", 22000, true);
      UseTaxComparator uC = new UseTaxComparator();
   
      Assert.assertEquals("v1 tax > v2 tax Pass", 1, uC.compare(v1, v2));
   }             
}
