import java.io.FileNotFoundException;
/**
 * A VehiclesPart3 class containing the main method.
 * 
 * Project 6
 *
 * @author Karn Kausal - CPSC 1223 - A01
 * @version 09262019
 */
public class VehiclesPart3 {

   /**
    * Main method to test the classes.
    * @param args Command line arguments.
    */
   public static void main(String [] args) {
   
      /* if the user runs the program without a command line argument,
       *  display message.
       */
      if (args.length == 0) {
         
         System.out.println("*** File name not provided by command line" 
               + " argument.\nProgram ending.\n");
               
      }
      else
      {                  
      /* get the file name from the command line, read in the data from 
       * the file and populate the list.
       */
         UseTaxList u = new UseTaxList(); 
      
         try {
         
            u.readVehicleFile(args[0]);
         
         /* Print the summary, the vehicle list by owner, the vehicle list by 
         * use tax, and the list of excluded records.
         */
         
            System.out.println(u.summary());
            System.out.println("\n" + u.listByOwner());
            System.out.println("\n" + u.listByUseTax());
            System.out.println("\n" + u.excludedRecordsList());
         
         }
         catch (FileNotFoundException e) {
         
            System.out.println("*** File not found.\nProgram ending.\n");
         
         }
      
      } // end else clause
         
   } // end method main

} // end class