import java.util.Scanner;
import java.io.FileNotFoundException;
import java.io.File;
import java.util.Arrays;
import java.text.DecimalFormat;
import java.util.NoSuchElementException;

/**
 * A UseTaxList class provides methods for reading in data and 
 * generating reports (summary and list), adding a Vehicle, and 
 * sorting the vehicles by owner and by use tax amount.
 * 
 * Project 5
 *
 * @author Karn Kausal - CPSC 1223 - A01
 * @version 09192019
 */
public class UseTaxList {

   //------------------------instance variables----------------------//
   
   // entity in charge of the use tax list      
   private String taxDistrict;
   

   private Vehicle [] vehicleList;      
   private String [] excludedRecords;

//------------------------constructors----------------------------//

   /**
    * Constructor to set the values.
    */
   public UseTaxList() {
   
      taxDistrict = "not yet assigned";
      vehicleList = new Vehicle[0];
      excludedRecords = new String[0];
   
   } // end Constructor


   /**
    * Read in the file and then read it in line by line.
    * @param fileName represents the name of the file
    * @throws FileNotFoundException from scanning input file.
    */
   public void readVehicleFile(String fileName) 
         throws FileNotFoundException {
   
      String line = "";
      boolean taxDistrictScan = false;
         
      // read in the file
      Scanner file = new Scanner(new File(fileName));            
               
      // process the file
      while (file.hasNext()) {            
      
         try {
         
            if (!taxDistrictScan) {
               
               // only read the tax district once
               this.taxDistrict = file.nextLine();
               taxDistrictScan = true;
            
            }
            // read in the lines/contents of the file
            line = file.nextLine();
         
            // create second scanner to read the vehicles
            Scanner readVehicle = new Scanner(line);
            
            // set the delimiter
            readVehicle.useDelimiter(";");
         
         
            String category = readVehicle.next().trim();
            String name = readVehicle.next().trim();
            String yearMakeModel = readVehicle.next().trim();
            double value = Double.parseDouble(readVehicle.next().trim());
            boolean altFuel = Boolean.parseBoolean(readVehicle.next().trim());
         
                        
         // retrieve the first character to determine the category of vehicle.
            char typeOfVehicle = category.toUpperCase().charAt(0);
         
                        
            switch (typeOfVehicle) {
            
               case 'C':
                  // create a vehicle and add it.
                  this.addVehicle(new Car(name, yearMakeModel, value, altFuel));
                                    
                  break;
            
               case 'T':
                  double tons = Double.parseDouble(readVehicle.next().trim());
               
               // create a vehicle and add it.
                  this.addVehicle(new Truck(name, yearMakeModel, value, 
                     altFuel, tons));
               
                  break;
            
               case 'S':
                  double semiTons = 
                     Double.parseDouble(readVehicle.next().trim());
                  int axles = Integer.parseInt(readVehicle.next().trim());    
               
               // create a vehicle and add it.
                  this.addVehicle(
                     new SemiTractorTrailer(
                     name, yearMakeModel, value, altFuel, semiTons, axles));
               
                  break;            
            
               case 'M':
                  double engineSize = 
                     Double.parseDouble(readVehicle.next().trim());
               
               // create a vehicle and add it.
                  this.addVehicle(
                     new Motorcycle(
                     name, yearMakeModel, value, altFuel, engineSize));
               
                  break; 
               
               default:
               // add to the excluded records list
                  this.addExcludedRecord("Invalid Vehicle Category in:\n" 
                     + line);
                  break;
               
            } // end switch
         } // end try block
         // catch and handle exception for negative values
         catch (NegativeValueException e) {
               
            this.addExcludedRecord(e + " in:\n" + line);
               
         }
         // catch and handle exception for invalid numeric values
         catch (NumberFormatException e) {
               
            this.addExcludedRecord(e + " in:\n" + line);
               
         }  
         // catch and handle exception for missing values
         catch (NoSuchElementException e) {
               
            this.addExcludedRecord(e + " in:\n" + line);
               
         }                                      
         
      } // end loop
      
      // close the file
      file.close();
      
   } // end method readVehicleFile
   
   /**
    * @return String represents the name of the tax district.
    */
   public String getTaxDistrict() {
      
      return this.taxDistrict;
      
   } // end method getTaxDistrict

   /**
    * Set the name of the tax district.
    * @param taxDistrictIn represents the name of the tax district.
    */
   public void setTaxDistrict(String taxDistrictIn) {
      
      this.taxDistrict = taxDistrictIn;
      
   } // end method setTaxDistrict

   
   /**
    * @return Vehicle array.
    */
   public Vehicle [] getVehicleList() {
      
      return this.vehicleList;
      
   } // end method getVehicleList

   /**
    * @return Excluded Records list.
    */
   public String [] getExcludedRecords() {
      
      return this.excludedRecords;
      
   } // end method getExcludedRecords


   /**
    * Add a vehicle and increases the capacity of the vehicle list.
    * @param vehicleIn represents a vehicle.
    */
   public void addVehicle(Vehicle vehicleIn) {
      
      // copy the list to a new array with increased capcacity
      this.vehicleList = 
         Arrays.copyOf(this.vehicleList, this.vehicleList.length + 1);
         
         // add vehicle to the list.
      this.vehicleList[this.vehicleList.length - 1] = vehicleIn;
      
      
   } // end method addVehicle

   /**
    * Adds vehicle to excluded record list and increases the capacity of list.
    * @param excludedRecordIn represents the excluded vehicle.
    */
   public void addExcludedRecord(String excludedRecordIn) {
      
      // copy the list to a new array with increased capcacity
      this.excludedRecords = 
         Arrays.copyOf(this.excludedRecords, this.excludedRecords.length + 1);
         
      // add to the list.
      this.excludedRecords[this.excludedRecords.length - 1] 
         = excludedRecordIn;
      
      
   } // end method addExcludedRecord
    
   /**
    * @return String prints the list of vehicles.
    */
   public String toString() {
   
      String output = "";
      
      // For each vehicle in the list, print vehicle information.
      for (Vehicle v : vehicleList) {
      
         output += "\n" + v + "\n";
         
      }
      
      return output;
      
   }  // end method toString
   
   /**
    * @return double representing the total use tax for all of the 
    * vehicles in the list.
    */      
   public double calculateTotalUseTax() {
      
      double totalTax = 0;
      
      // for each vehicle in the list, add vehicle tax to the total.
      for (Vehicle v : vehicleList) {
      
         totalTax += v.useTax();
      
      } // end loop         
         
      return totalTax;
      
   } // end method calculateTotalUseTax
   
   /**
    * @return double representing the total use value for all of the 
    * vehicles in the list.
    */      
   public double calculateTotalValue() {
      
      double totalValue = 0;
      
      // for each vehicle in the list, add value to the total.
      for (Vehicle v : vehicleList) {
      
         totalValue += v.getValue();
      
      } // end loop         
         
      return totalValue;
      
   } // end method calculateTotalValue


   /**
    * @return String representing summary information for the tax district.
    */
   public String summary() {
   
      String summary = "";
      DecimalFormat df = new DecimalFormat("$#,##0.00");
      
      summary += "------------------------------"
         + "\nSummary for " + this.getTaxDistrict()
         + "\n------------------------------";
      summary += "\nNumber of Vehicles: " + this.getVehicleList().length
         + "\nTotal Value: " + df.format(this.calculateTotalValue())
         + "\nTotal Use Tax: " + df.format(this.calculateTotalUseTax())
         + "\n";
   
      return summary;
      
   }  // end method summary
   
   /**
    * @return String representing the vehicles list by 
    * owner (the natural sorting order).
    */
   public String listByOwner() {
   
      String title = "";
      
      // sort list by owner      
      Arrays.sort(vehicleList);  
      
      // create the title     
      title = "------------------------------\n";
      title += "Vehicles by Owner\n";
      title += "------------------------------\n";
      
      
      return (title + this.toString());   
                        
   } // end method listByOwner      

   /**
    * @return String representing the vehicles list sorted by use tax.
    */   
   public String listByUseTax() {
   
      String title = "";
   
      // sort list by use tax
      Arrays.sort(vehicleList, new UseTaxComparator());
         
      // create the title     
      title = "------------------------------\n";
      title += "Vehicles by Use Tax\n";
      title += "------------------------------\n";
            
       
      return (title + this.toString());
      
   } // end method listByUseTax 
      
      
   /**
    * @return String representing the list vehicles records/lines that 
    * were read from the file but excluded from the vehicles list.
    * @throws FileNotFoundException from scanning input file.
    */
   public String excludedRecordsList() {
      
      String excludedList = "";
   
      excludedList = "------------------------------\n";
      excludedList += "Excluded Records\n";
      excludedList += "------------------------------\n";
      
      // for each record in the excluded records list, print the record.
      for (String record : excludedRecords) {
      
         excludedList += "\n" + record + "\n";
         
      } // end loop
      
      return excludedList;
            
   } // end method excludedRecordsList      
   
} // end class    
