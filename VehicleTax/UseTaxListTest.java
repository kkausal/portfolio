import org.junit.Assert;
//import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;
import java.io.FileNotFoundException;

/**
 * A JUnit test file to test the methods in the UseTaxList class.
 */   
public class UseTaxListTest {


   /** Fixture initialization (common initialization
    *  for all tests). **/
   @Before public void setUp() {
   }


   /** Test readFile method. 
    * @throws FileNotFoundException from scanning input file.
    */
   @Test public void readFileTest() 
      throws FileNotFoundException {
   
      String fileName = "vehicles1.txt";
      UseTaxList u = new UseTaxList();
      u.readVehicleFile(fileName);
      
      // test by ensuring number of vehicles is correct.
      Assert.assertEquals("readFile Pass", 8, u.getVehicleList().length);
   }
   
   /** Test getTaxDistrict method. 
    * @throws FileNotFoundException from scanning input file.
    */
   @Test public void getTaxDistrictTest() 
      throws FileNotFoundException {
   
      String fileName = "vehicles1.txt";
      UseTaxList u = new UseTaxList();
      u.readVehicleFile(fileName);
      
      Assert.assertEquals("Tax District Pass", "Tax District 52", 
         u.getTaxDistrict());
   }
   
   /** Test setTaxDistrict method. 
    * @throws FileNotFoundException from scanning input file.
    */
   @Test public void setTaxDistrictTest() 
      throws FileNotFoundException {
   
      String fileName = "vehicles1.txt";
      UseTaxList u = new UseTaxList();
      u.readVehicleFile(fileName);
      u.setTaxDistrict("District 9");
      
      Assert.assertEquals("set District Pass", "District 9", 
         u.getTaxDistrict());
   }
   
   /** Test getVehicleList method. 
    * @throws FileNotFoundException from scanning input file. 
    * @throws NegativeValueException from scanning negative input data.
    */
   @Test public void getVehicleListTest() 
      throws FileNotFoundException, NegativeValueException {
   
      String fileName = "vehicles1.txt";
      Vehicle [] v = new Vehicle[8];
      
      UseTaxList u = new UseTaxList();
      
      v[0] = new Car("Jones, Sam", "2014 Honda Accord", 22000, false);
      v[1] = new Car("Jones, Jo", "2014 Honda Accord", 22000, true);
      v[2] = new Car("Smith, Pat", "2015 Mercedes-Benz Coupe", 110000, false);
      v[3] = new Car("Smith, Jack", "2015 Mercedes-Benz Coupe", 110000, true);
      v[4] = 
         new Truck("Williams, Jo", "2012 Chevy Silverado", 30000, false, 1.5);
      v[5] = new Truck("Williams, Sam", "2010 Chevy Mack", 40000, true, 2.5);
      v[6] = 
            new SemiTractorTrailer("Williams, Pat", 
               "2012 International Big Boy", 45000, false, 5.0, 4);         
      v[7] = 
         new Motorcycle("Brando, Marlon", "1964 Harley-Davidson Sportster", 
            14000, false, 750);
      
      u.readVehicleFile(fileName);
      
      
      Assert.assertArrayEquals("get Vehicle List Pass", v, 
         u.getVehicleList());
   } 
   
   /** Test getExcludedRecords method. 
    * @throws FileNotFoundException from scanning input file.
    * @throws NegativeValueException from scanning negative input data.       
    */
   @Test public void getExcludedRecordsTest() 
      throws FileNotFoundException, NegativeValueException {
   
      String fileName = "vehicles1.txt";
      String [] v = new String[2];
      
      v[0] = "Invalid Vehicle Category in:\n"
         + "race car; Zinc, Zed; 2013 Custom Hot Rod; 61000; false";
      v[1] = "Invalid Vehicle Category in:\n"
         + "Firetruck; Body, Abel; 2015 GMC FE250; 55000; false; 2.5";
      
      UseTaxList u = new UseTaxList();
      
      u.readVehicleFile(fileName);
   
      Assert.assertArrayEquals("get Excluded Records Pass", v, 
         u.getExcludedRecords());
            
                            
   }
   
   /** Test addVehicle method.  
    * @throws NegativeValueException from scanning negative input data.
    */
   @Test public void addVehicleTest() throws NegativeValueException {
   
      Vehicle [] v = new Vehicle[2];
      
      UseTaxList u = new UseTaxList();
      
      v[0] = new Car("Wayne, Bruce", "2017 Bat Mobile", 22000, false);
      v[1] = new Motorcycle("Wilder, Gene", "1964 Silver Streak", 
            14000, false, 750);
      
      u.addVehicle(new Car("Wayne, Bruce", "2017 Bat Mobile", 22000, false));
      u.addVehicle(new Motorcycle("Wilder, Gene", "1964 Silver Streak", 
            14000, false, 750));
   
      Assert.assertArrayEquals("add vehicle Pass", v, 
         u.getVehicleList());
   
         
   }
   
   
   /** Test addExcludedRecords method.  
    * @throws NegativeValueException from scanning negative input data.
    */
   @Test public void addExcludedRecords() throws NegativeValueException {
   
      String [] v = new String[2];
      
      UseTaxList u = new UseTaxList();
      
      v[0] = "slow car; Zinc, Zed; 2013 Custom Hot Rod; 61000; false";
      v[1] = "Huge Truck; Body, Abel; 2015 GMC FE250; 55000; false; 2.5";
      
      u.addExcludedRecord(
         "slow car; Zinc, Zed; 2013 Custom Hot Rod; 61000; false");
      u.addExcludedRecord(
         "Huge Truck; Body, Abel; 2015 GMC FE250; 55000; false; 2.5");
   
      Assert.assertArrayEquals("add excluded Record Pass", v, 
         u.getExcludedRecords());
   
         
   }  
   
   /** Test toString method. 
    * @throws FileNotFoundException from scanning input file. 
    * @throws NegativeValueException from scanning negative input data.
    */
   @Test public void toStringTest() 
      throws FileNotFoundException, NegativeValueException {
   
      String fileName = "vehicles1.txt";
      String output = "";
      Vehicle [] vList = new Vehicle[8];
      
      UseTaxList u = new UseTaxList();
      
      vList[0] = new Car("Jones, Sam", "2014 Honda Accord", 22000, false);
      vList[1] = new Car("Jones, Jo", "2014 Honda Accord", 22000, true);
      vList[2] = 
         new Car("Smith, Pat", "2015 Mercedes-Benz Coupe", 110000, false);
      vList[3] = 
         new Car("Smith, Jack", "2015 Mercedes-Benz Coupe", 110000, true);
      vList[4] = 
         new Truck("Williams, Jo", "2012 Chevy Silverado", 30000, false, 1.5);
      vList[5] = 
         new Truck("Williams, Sam", "2010 Chevy Mack", 40000, true, 2.5);
      vList[6] = 
            new SemiTractorTrailer("Williams, Pat", 
               "2012 International Big Boy", 45000, false, 5.0, 4);         
      vList[7] = 
         new Motorcycle("Brando, Marlon", "1964 Harley-Davidson Sportster", 
            14000, false, 750);
      
      u.readVehicleFile(fileName);
      
      for (Vehicle v : vList) {
         
         output += "\n" + v + "\n";
         
      } 
      
      
      Assert.assertEquals("toString Pass", output, u.toString());
   } 
       

   /** Test calculateTotalUseTax method.  
    * @throws NegativeValueException from scanning negative input data.
    */
   @Test public void calculateTotalUseTaxTest() 
      throws NegativeValueException {
   
      Vehicle [] v = new Vehicle[2];
      double totalUseTax = 0;
      
      UseTaxList u = new UseTaxList();
      
      v[0] = new Car("Wayne, Bruce", "2017 Bat Mobile", 22000, false);
      v[1] = new Motorcycle("Wilder, Gene", "1964 Silver Streak", 
            14000, false, 750);
      
      u.addVehicle(new Car("Wayne, Bruce", "2017 Bat Mobile", 22000, false));
      u.addVehicle(new Motorcycle("Wilder, Gene", "1964 Silver Streak", 
            14000, false, 750));
   
      totalUseTax = v[0].useTax() + v[1].useTax();
      Assert.assertEquals("calculateTotalUseTax Pass", totalUseTax, 
         u.calculateTotalUseTax(), 0.000001);
   
         
   }
   
   /** Test calculateTotalValue method.  
    * @throws NegativeValueException from scanning negative input data.
    */
   @Test public void calculateTotalValueTest() throws NegativeValueException {
   
      Vehicle [] v = new Vehicle[2];
      double totalUseTax = 0;
      
      UseTaxList u = new UseTaxList();
      
      // Add vehicles to v array
      v[0] = new Car("Wayne, Bruce", "2017 Bat Mobile", 22000, false);
      v[1] = new Motorcycle("Wilder, Gene", "1964 Silver Streak", 
            14000, false, 750);
      
      // add vehicles to the vehicle list in UseTaxList
      u.addVehicle(new Car("Wayne, Bruce", "2017 Bat Mobile", 22000, false));
      u.addVehicle(new Motorcycle("Wilder, Gene", "1964 Silver Streak", 
            14000, false, 750));
      
      // compute total use tax for vehicles in the v array
      totalUseTax = v[0].getValue() + v[1].getValue();
      Assert.assertEquals("calculateTotalValue Pass", totalUseTax, 
         u.calculateTotalValue(), 0.000001);
   
         
   }

   /** Test summary method. 
    * @throws FileNotFoundException from scanning input file.
    * @throws NegativeValueException from scanning negative input data.    
    */
   @Test public void summaryTest() 
      throws FileNotFoundException, NegativeValueException {
   
      String fileName = "vehicles1.txt";
      String output = "";
      Vehicle [] vList = new Vehicle[8];
      String summary = "";
      
      UseTaxList u = new UseTaxList();
      
      vList[0] = new Car("Jones, Sam", "2014 Honda Accord", 22000, false);
      vList[1] = new Car("Jones, Jo", "2014 Honda Accord", 22000, true);
      vList[2] = 
         new Car("Smith, Pat", "2015 Mercedes-Benz Coupe", 110000, false);
      vList[3] = 
         new Car("Smith, Jack", "2015 Mercedes-Benz Coupe", 110000, true);
      vList[4] = 
         new Truck("Williams, Jo", "2012 Chevy Silverado", 30000, false, 1.5);
      vList[5] = 
         new Truck("Williams, Sam", "2010 Chevy Mack", 40000, true, 2.5);
      vList[6] = 
            new SemiTractorTrailer("Williams, Pat", 
               "2012 International Big Boy", 45000, false, 5.0, 4);         
      vList[7] = 
         new Motorcycle("Brando, Marlon", "1964 Harley-Davidson Sportster", 
            14000, false, 750);
      
      u.readVehicleFile(fileName);
      
               
               
      summary += "------------------------------"
         + "\nSummary for " + "Tax District 52"
         + "\n------------------------------";
      summary += "\nNumber of Vehicles: " + 8
         + "\nTotal Value: " + "$" + "393,000.00"
         + "\nTotal Use Tax: " + "$" + "12,010.00"
         + "\n";
            
      
      
      Assert.assertEquals("summary Pass", summary, u.summary());
   } 
   
   /** Test listByOwner method.  
    * @throws NegativeValueException from scanning negative input data.
    */
   @Test public void listByOwnerTest() throws NegativeValueException {
      
      String title;
      Vehicle [] v = new Vehicle[2];
      
      UseTaxList u = new UseTaxList();
      
      // Add vehicles to v array in natural order
      v[0] = new Car("Wayne, Bruce", "2017 Bat Mobile", 22000, false);
      v[1] = new Motorcycle("Wilder, Gene", "1964 Silver Streak", 
            14000, false, 750);
      
      // add vehicles to UseTaxList but add them out of natural order 
      u.addVehicle(new Motorcycle("Wilder, Gene", "1964 Silver Streak", 
            14000, false, 750));
      u.addVehicle(new Car("Wayne, Bruce", "2017 Bat Mobile", 22000, false));
   
   
      // create the title     
      title = "------------------------------\n";
      title += "Vehicles by Owner\n";
      title += "------------------------------\n";
      
      title += "\n" + v[0].toString() + "\n"; 
      title += "\n" + v[1].toString() + "\n";
               
         // sort the array in natural order and compare it with the v array.
      Assert.assertEquals("listByOwner Pass", title, u.listByOwner());
      
         
   }

   /** Test listByUseTax method.  
    * @throws NegativeValueException from scanning negative input data.
    */
   @Test public void listByUseTax() throws NegativeValueException {
         
      String title;
      Vehicle [] v = new Vehicle[2];
      
      UseTaxList u = new UseTaxList();
      
      // Add vehicles to v array where Car has less use tax than motorcycle.
      v[0] = new Car("Jones, Jo", "2014 Honda Accord", 22000, true);
      v[1] = new Motorcycle("Brando, Marlon", "1964 Harley-Davidson Sportster", 
            14000, false, 750);
      
      // add vehicles to UseTaxList but add them where use tax for motorcycle,
      // which is higher comes before the car use tax (that is less).
      u.addVehicle(
         new Motorcycle("Brando, Marlon", "1964 Harley-Davidson Sportster", 
            14000, false, 750));
      u.addVehicle(new Car("Jones, Jo", "2014 Honda Accord", 22000, true));
   
   
      // create the title     
      title = "------------------------------\n";
      title += "Vehicles by Use Tax\n";
      title += "------------------------------\n";
      
      title += "\n" + v[0].toString() + "\n"; 
      title += "\n" + v[1].toString() + "\n";
               
      // sort the array based on use tax and compare the string values.
      Assert.assertEquals("listByUseTax Pass", title, u.listByUseTax());
      
         
      
   }

   /** Test excludedRecordsList method.  
    * @throws NegativeValueException from scanning negative input data.
    */
   @Test public void excludedRecordsListTest() throws NegativeValueException {
   
      String [] v = new String[2];
      String excludedList = "";
      
      v[0] = "race car; Zinc, Zed; 2013 Custom Hot Rod; 61000; false";
      v[1] = "Firetruck; Body, Abel; 2015 GMC FE250; 55000; false; 2.5";
      
      UseTaxList u = new UseTaxList();
      
      u.addExcludedRecord(
         "race car; Zinc, Zed; 2013 Custom Hot Rod; 61000; false");
      u.addExcludedRecord(
         "Firetruck; Body, Abel; 2015 GMC FE250; 55000; false; 2.5");
            
   
      excludedList = "------------------------------\n";
      excludedList += "Excluded Records\n";
      excludedList += "------------------------------\n";
               
      excludedList += "\n" + v[0] + "\n";
      excludedList += "\n" + v[1] + "\n";
         
   
      Assert.assertEquals("Excluded Records List Pass", excludedList, 
         u.excludedRecordsList());
            
                            
   }   
   
}
