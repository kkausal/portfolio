**Overview:** 

Employed Polymorphism, Exceptions & Java’s IO classes to design a complex program to read a long text 

file containing vehicle data using data structures and performed calculations.

**Structure of this program:**

This project calculates the annual use tax for vehicles where the amount is based on the type of vehicle, 

its value, and various tax rates. The project was developed in three parts as follows:

*Part 1:*

- Developed Java classes that represented categories of vehicles: car, truck, semi-tractor trailer (a subclass of truck), and motorcycle. 

*Part 2 implemented three additional classes:* 

1. UseTaxComparator that implemented the Comparator interface.
2. UseTaxList that represented a list of vehicles and includes several specialized methods.
3. The main method for the program that created a Vehicle object, read the data file using the readVehicleFile method, printed a summary, a vehicles list by owner and by use tax, and the list of excluded records. 

*Part 3 implemented exception handing and invalid input reporting as follows:*

1. Created a new class named NegativeValueException which extends the Exception class.

2. Added try-catch statements to catch FileNotFoundException in the main method of the VehiclesPart3 class.

3. Modified the readVehicleFile in the UseTaxList class to catch/handle NegativeValueException, NumberFormatException, and NoSuchElementException in the event that these type exceptions are thrown while reading the input file. 

   Note that the main method inVehiclesPart3 created a UseTaxList object and then invoked the readVehicleFile method on the UseTaxList object to read data from a file and add vehicles to the vehicleList array in the UseTaxList object. 

**Instructions to run this program:**

The provided vehicles2.txt is required as an argument to the main method. Therefore, when running in 

an IDE, vehicles2.txt should be placed in the “Run Arguments” section before running the main method. 

Or, if running the VehiclePart3.class through the command line:

java VehiclePart3 vehicles2.txt

