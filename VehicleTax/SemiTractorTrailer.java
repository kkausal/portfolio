/**
 * A SemiTractorTrailer class dervied from Vehicle.
 * 
 * Project 4
 *
 * @author Karn Kausal - CPSC 1223 - A01
 * @version 09122019
 */
public class SemiTractorTrailer extends Truck {
 
   //------------------------constants-------------------------------//
   /** 
    * Public constants.
    */
   public static final double PER_AXLE_TAX_RATE = 0.005; 

   //------------------------instance variables----------------------//
 
   protected int axles;

   //------------------------constructors----------------------------//
   /**
    * Constructor to set the values for the Semi Tractor Trailer.
    * @param ownerIn represents the vehicle's owner.
    * @param yearMakeModelIn represents the vehicle's year, make and model.
    * @param valueIn represents value of the vehicle.
    * @param altFuelIn represents alternative fuel.
    * @param tonsIn represents the tons.
    * @param axlesIn represents the axles.
    * @throws NegativeValueException from scanning negative input data.    
    */
   public SemiTractorTrailer(String ownerIn, String yearMakeModelIn, 
      double valueIn, boolean altFuelIn, double tonsIn, int axlesIn) throws 
                     NegativeValueException {
         
      super(ownerIn, yearMakeModelIn, valueIn, altFuelIn, tonsIn);
   
      // throw exception if negative data is received         
      if (axlesIn < 0) {
         
         vehicleCount--;
      
         throw new NegativeValueException();
      
      }          
      this.axles = axlesIn;
      
   } // end Constructor

   //------------------------methods---------------------------------//
    
   
   /**
    * @return int representing the Semi Tractor Trailer's axles.
    */
   public int getAxles() {
   
      return this.axles;
   
   } // end method getAxles
 
   /**
    * @param axlesIn represents Semi Tractor Trailer's axles.
    */
   public void setAxles(int axlesIn) {
    
      this.axles = axlesIn;
    
   } // end method setAxles

  /**
   * Override the useTax method in the Truck class.
   * @return double representing the total use tax.
   */
   public double useTax() {
      
      double totalUseTax = 0;
      
      totalUseTax = super.useTax() 
         + (this.value * PER_AXLE_TAX_RATE * this.axles);
      
      
      
      return totalUseTax;
      
   
   } // end method useTax

   /**
     * 
     * @return String describing the Semi Tractor Trailer.
     */      
   public String toString() {
      
      String output = "";
         
      output = super.toString() + " Axle Tax Rate: " 
         + PER_AXLE_TAX_RATE * this.axles;
      
             
         
      return output;
                        
      
   } // end method toString.      
      
 
} // end class.
