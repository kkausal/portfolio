import org.junit.Assert;
//import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;
import java.text.DecimalFormat;

 /**
  * A JUnit test file to test the methods in the Motorcycle class.
  */   
public class MotorcycleTest {

   static final double TAX_RATE = 0.005, 
                              ALTERNATIVE_FUEL_TAX_RATE = 0.0025, 
                              LARGE_BIKE_CC_THRESHOLD = 499, 
                              LARGE_BIKE_TAX_RATE = 0.015;


   /** Fixture initialization (common initialization
    *  for all tests). **/
   @Before public void setUp() {
   }

   /** Test getEngineSize method.  
    * @throws NegativeValueException from scanning negative input data.
    */
   @Test public void getEngineSizeTest() throws NegativeValueException {
      
      Motorcycle bike1 = new Motorcycle("Brando, Marlon",
            "1964 Harley-Davidson Sportster", 14000, false, 750);
   
                  
      Assert.assertEquals("getEngineSize Passed", 750, bike1.getEngineSize(), 
         0.000001);
   }
   
   /** Test setEngineSize method in Vehicle.  
    * @throws NegativeValueException from scanning negative input data.
    */
   @Test public void setEngineSizeTest() throws NegativeValueException {
      
      Motorcycle bike1 = new Motorcycle("Brando, Marlon",
            "1964 Harley-Davidson Sportster", 14000, false, 750);
      
      bike1.setEngineSize(100);
                              
      Assert.assertEquals("setEngineSize Passed", 100, bike1.getEngineSize(), 
         0.000001);
   }   
   
   /** Test toString method in Motorcycle.  
    * @throws NegativeValueException from scanning negative input data.
    */
   @Test public void toStringTest() throws NegativeValueException {
   
                     
      String output = "";
      DecimalFormat df = new DecimalFormat("$#,##0.00");
      
      /** Test with no alternative fuel & engine size does not exceed 
       *  threshold.
       */
      Motorcycle bike1 = new Motorcycle("Brando, Marlon",
            "1964 Harley-Davidson Sportster", 14000, false, 400);
                    
      output = bike1.getOwner() + ": " + bike1.getClass().getName() 
             + " " + bike1.yearMakeModel;
         
      output += "\n" + "Value: " + df.format(bike1.getValue()) 
             + " Use Tax: " + df.format(bike1.useTax());
      
      output += "\n" + "with Tax Rate: " + TAX_RATE;
                  
      Assert.assertEquals("toString Passed", output, bike1.toString());
   
                  
   
   }    
   
   /** Test toString method with Alt Fuel & Threshold.  
    * @throws NegativeValueException from scanning negative input data.
    */
   @Test public void toStringAltFuelTest() throws NegativeValueException {
   
      String outputAltFuel = "";
      DecimalFormat df = new DecimalFormat("$#,##0.00");
         
   
      /** Test with alternative fuel & & engine size exceed threshold. **/
   
      Motorcycle bike1 = new Motorcycle("Brando, Marlon",
            "1964 Harley-Davidson Sportster", 14000, true, 750);
   
      
      
      outputAltFuel = bike1.getOwner() + ": " + bike1.getClass().getName() 
             + " " + bike1.yearMakeModel + " (Alternative Fuel)";
         
      outputAltFuel += "\n" + "Value: " + df.format(bike1.getValue()) 
             + " Use Tax: " + df.format(bike1.useTax());
      
      outputAltFuel += "\n" + "with Tax Rate: " + ALTERNATIVE_FUEL_TAX_RATE;
   
      outputAltFuel += " Large Bike Tax Rate: " + LARGE_BIKE_TAX_RATE;
                             
   
                  
      Assert.assertEquals("toString Passed", outputAltFuel, bike1.toString());
      
   }

   /** Test useTax method.  
    * @throws NegativeValueException from scanning negative input data.
    */
   @Test public void useTaxTest() throws NegativeValueException {
   
      Motorcycle bike1 = new Motorcycle("Brando, Marlon",
            "1964 Harley-Davidson Sportster", 14000, false, 750);
      
      Assert.assertEquals("useTax test", 280.0, bike1.useTax(), 0.000001);
   
   }              

   

}
