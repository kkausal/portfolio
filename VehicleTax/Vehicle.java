import java.text.DecimalFormat;

/**
 * In addition to the specifications in Part 1 (Project 4), 
 * the Vehicle class implements the Comparable interface for Vehicle objects.
 * 
 * A Vehicle class that represent categories of vehicles: car, truck,
 * semi-tractor trailer (a subclass of truck), and motorcycle.
 * 
 * Project 6
 *
 * @author Karn Kausal - CPSC 1223 - A01
 * @version 09262019
 */
public abstract class Vehicle implements Comparable<Vehicle> {
 
   //------------------------instance variables----------------------//
   
   // vehicle owner, year, make, model, value, alternative fuel
   protected String owner;
   protected String yearMakeModel;
   protected double value;
   protected boolean altFuel;
   

   //------------------------class variables-------------------------//
   /* track the number of vehicles that are created from the classes in 
    * the Vehicle hierarchy
    */  
   protected static int vehicleCount;


   //------------------------constructors----------------------------//

   /**
    * Constructor to set the values for the vehicle.
    * @param ownerIn represents the vehicle's owner.
    * @param yearMakeModelIn represents the vehicle's year, make and model.
    * @param valueIn represents value of the vehicle.
    * @param altFuelIn represents alternative fuel.
    * @throws NegativeValueException from scanning negative input data.     
    */
   public Vehicle(String ownerIn, String yearMakeModelIn, double valueIn, 
                  boolean altFuelIn) throws NegativeValueException {
      
      // throw exception if negative data is received
      if (valueIn < 0) {
      
         throw new NegativeValueException();
      
      }         
      this.owner = ownerIn;
      this.yearMakeModel = yearMakeModelIn;
      this.altFuel = altFuelIn;
      this.value = valueIn;
      
      
      vehicleCount++;
                              
   } // end Constructor


   //------------------------methods---------------------------------//

   /**
    * @return String representing the Vehicle's owner
    */
   public String getOwner() {
   
      return this.owner;
   
   } // end method getOwner
 
   /**
    * @param ownerIn represents the Vehicle's owner.
    */
   public void setOwner(String ownerIn) {
    
      this.owner = ownerIn;
    
   } // end method setOwner
    
   /**
    * @return String representing the Vehicle's year, make & model.
    */
   public String getYearMakeModel() {
    
      return this.yearMakeModel;
    
   } // end method getYearMakeModel


   /**
    * @param yearMakeModelIn represents the Vehicle's year, make & model.
    */
   public void setYearMakeModel(String yearMakeModelIn) {
    
      this.yearMakeModel = yearMakeModelIn;
    
   } // end method setYearMakeModel
   

   /**
    * @return double representing the Vehicle's value.
    */
   public double getValue() {
   
      return this.value;
   
   } // end method getValue
 
   /**
    * @param valueIn represents the Vehicle's value.
    */
   public void setValue(double valueIn) {
    
      this.value = valueIn;
    
   } // end method setValue
      

   /**
    * @return boolean representing the alternative fuel.
    */
   public boolean getAlternativeFuel() {
   
      return this.altFuel;
   
   } // end method getAlternativeFuel
 
   /**
    * @param altFuelIn represents alternative fuel.
    */
   public void setAlternativeFuel(boolean altFuelIn) {
    
      this.altFuel = altFuelIn;
    
   } // end method setAlternativeFuel
   
   /**
    * @return int representing the count for the number of vehicles created.
    */
   public static int getVehicleCount() {
   
      return vehicleCount;
   
   } // end method getVehicleCount
   
   /**
    * Resets the Vehicle's count to zero.
    */
   public static void resetVehicleCount() {
    
      vehicleCount = 0;
    
   } // end method resetVehicleCount
   
   /**
    * 
    * @return String describing the Vehicle.
    */      
   public String toString() {
    
      String output = "";
      DecimalFormat df = new DecimalFormat("$#,##0.00");
      
      output = this.getOwner() + ": " + this.getClass().getName() 
             + " " + this.yearMakeModel;
         
         // if Vehicle has alternative fuel, add that to the String.
      if (this.altFuel) {
         
         output += " (Alternative Fuel)";
         
      }
         
      output += "\n" + "Value: " + df.format(this.getValue()) 
             + " Use Tax: " + df.format(this.useTax());
             
      return output;                
    
   } // end method toString
   
   /**
    * Compares two Vehicle's equality.
    * @param obj represents the other object.
    * @return boolean true if this Vehicle owner, yearMakeModel, and 
    * value are all equal to the Object parameter’s vehicle owner, 
    * yearMakeModel, and value; otherwise returns false.
    * 
    */
   public boolean equals(Object obj) {
   
      // if the object is not an instance of vehicle, cannot compare.
      if (!(obj instanceof Vehicle)) {
         return false;
      } 
      else {
      
         Vehicle other = (Vehicle) obj;
         return (owner + yearMakeModel + value).
                  equals(other.owner + other.yearMakeModel + other.value);
      
      }
   
   }  // end method equals
   
   /**
    * Overrides the hashCode method inherited from the Object class.
    * @return 0
    */
   public int hashCode() {
      
      return 0;
      
   } // end method hashCode
   
   /**
    * compareTo method added for Part 2 (Project 5).
    * Compares Vehicle objects based on their respective owners.
    * 
    * @param vehicleIn represents the vehicle used for comparison.
    * @return int indicating the results of comparing Vehicle objects based 
    * on their respective owners.
    */
   public int compareTo(Vehicle vehicleIn) {
      
      return this.owner.toLowerCase()
         .compareTo(vehicleIn.getOwner().toLowerCase());
      
   }  // end method compareTo

   //------------------------abstract methods------------------------//
   
   /**
    * @return double representing the total amount for the vehicle’s use tax.
    * Will be overriden in subclasses.
    */
   public abstract double useTax();
   
    
} // end class 
