/**
 * A Truck class dervied from Vehicle.
 * 
 * Project 4
 *
 * @author Karn Kausal - CPSC 1223 - A01
 * @version 09122019
 */
public class Truck extends Vehicle {
 
   //------------------------constants-------------------------------//
   /** 
    * Public constants.
    */
   public static final double TAX_RATE = 0.02, 
                              ALTERNATIVE_FUEL_TAX_RATE = 0.01, 
                              LARGE_TRUCK_TONS_THRESHOLD = 2.0, 
                              LARGE_TRUCK_TAX_RATE = 0.03; 

   //------------------------instance variables----------------------//
 
   protected double tons;

   //------------------------constructors----------------------------//
   /**
    * Constructor to set the values for the truck.
    * @param ownerIn represents the vehicle's owner.
    * @param yearMakeModelIn represents the vehicle's year, make and model.
    * @param valueIn represents value of the vehicle.
    * @param altFuelIn represents alternative fuel.
    * @param tonsIn represents the tons.
    * @throws NegativeValueException from scanning negative input data.    
    */
   public Truck(String ownerIn, String yearMakeModelIn, double valueIn, 
                  boolean altFuelIn, double tonsIn) throws 
                     NegativeValueException {
      super(ownerIn, yearMakeModelIn, valueIn, altFuelIn);
   
      // throw exception if negative data is received         
      if (tonsIn < 0) {
         
         vehicleCount--;
      
         throw new NegativeValueException();
      
      }          
      
      this.tons = tonsIn; 
                  
   } // end Constructor 
   
   //------------------------methods---------------------------------//
    
   
   /**
    * @return double representing the Truck's tons.
    */
   public double getTons() {
   
      return this.tons;
   
   } // end method getTons
 
   /**
    * @param tonsIn represents the Vehicle's owner.
    */
   public void setTons(double tonsIn) {
    
      this.tons = tonsIn;
    
   } // end method setTons
       
  /**
   * Implement the abstract method defined in the Vehicle class.
   * @return double representing the total use tax.
   */
   public double useTax() {
      
      double totalUseTax = 0;
      
      // check whether or not truck uses alternative fuel
      if (this.altFuel) {
         
         totalUseTax = this.value * ALTERNATIVE_FUEL_TAX_RATE;
      
      }
      else {
      
         totalUseTax = this.value * TAX_RATE;
      }
      
      /* if tons exceeds large truck tons threshold, add 
       * luxury rate to total use tax.
       */
      if (this.tons > LARGE_TRUCK_TONS_THRESHOLD) {
      
         totalUseTax += (this.value * LARGE_TRUCK_TAX_RATE);
      
      }
      
      return totalUseTax;
      
   
   } // end method useTax

   /**
     * 
     * @return String describing the Truck.
     */      
   public String toString() {
      
      String output = "";
         
      output = super.toString() + "\n" + "with Tax Rate: ";
      
      /* if Truck has alternative fuel, print the alt fuel tax rates.
       * Otherwise, print the regular tax rate. 
       */   
      if (this.altFuel) {
            
         output += ALTERNATIVE_FUEL_TAX_RATE; 
                
      }       
      else {
      
         output += TAX_RATE;
      } 
      
      // print the large truck tax rate if tons exceeds threshold.
      if (this.tons > LARGE_TRUCK_TONS_THRESHOLD) {
      
         output += " Large Truck Tax Rate: " + LARGE_TRUCK_TAX_RATE;            
      
      }
      
                          
         
      return output;
                        
      
   } // end method toString.    
   
} // end class 
