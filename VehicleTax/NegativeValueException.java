/**
 * A Division class that provides methods for performing 
 * integer & decimal division.
 * 
 * Project 6
 *
 * @author Karn Kausal - CPSC 1223 - A01
 * @version 09262019
 */
public class NegativeValueException extends Exception {

   //------------------------constructors----------------------------//
   
   /**
    * Constructor.
    */
   public NegativeValueException() {
      
      super("Numeric values must be nonnegative");
      
   }  // end Constructor
   
   
   
   
} // end class