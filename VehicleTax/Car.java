/**
 * A Car class dervied from Vehicle.
 * 
 * Project 4
 *
 * @author Karn Kausal - CPSC 1223 - A01
 * @version 09122019
 */
public class Car extends Vehicle {

   //------------------------constants-------------------------------//
   /** 
    * Public constants.
    */
   public static final double TAX_RATE = 0.01, 
                              ALTERNATIVE_FUEL_TAX_RATE = 0.005, 
                              LUXURY_THRESHOLD = 50_000, 
                              LUXURY_TAX_RATE = 0.02; 
   


   //------------------------constructors----------------------------//
   /**
    * Constructor to set the values for the car.
    * @param ownerIn represents the vehicle's owner.
    * @param yearMakeModelIn represents the vehicle's year, make and model.
    * @param valueIn represents value of the vehicle.
    * @param altFuelIn represents alternative fuel.
    * @throws NegativeValueException from scanning negative input data.    
    */
   public Car(String ownerIn, String yearMakeModelIn, double valueIn, 
                  boolean altFuelIn) throws NegativeValueException {
      
      super(ownerIn, yearMakeModelIn, valueIn, altFuelIn);
      
      
                           
   } // end Constructor    
   
   //------------------------methods---------------------------------//

   /**
    * Implement the abstract method defined in the Vehicle class.
    * @return double representing the total use tax.
    */
   public double useTax() {
      
      double totalUseTax = 0;
      
      // check whether or not car uses alternative fuel
      if (this.altFuel) {
         
         totalUseTax = this.value * ALTERNATIVE_FUEL_TAX_RATE;
      
      }
      else {
      
         totalUseTax = this.value * TAX_RATE;
      }
      
      // if value exceeds luxury threshold, add luxury rate to total use tax.
      if (this.value > LUXURY_THRESHOLD) {
      
         totalUseTax += (this.value * LUXURY_TAX_RATE);
      
      }
      
      return totalUseTax;
      
   
   } // end method useTax

   /**
     * 
     * @return String describing the Vehicle.
     */      
   public String toString() {
      
      String output = "";
         
      output = super.toString() + "\n" + "with Tax Rate: ";
      
      /* if Car has alternative fuel, print the alt fuel tax rates.
       * Otherwise, print the regular tax rate. 
       */    
      if (this.altFuel) {
            
         output += ALTERNATIVE_FUEL_TAX_RATE; 
                
      }
            
      else {
      
         output += TAX_RATE;
      } 
      
      // print the luxury tax rate if value exceeds threshold.
      if (this.value > LUXURY_THRESHOLD) {
      
         output += " Luxury Tax Rate: " + LUXURY_TAX_RATE;            
      
      }
      
                          
         
      return output;
                        
      
   } // end method toString.         

} // end class