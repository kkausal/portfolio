import org.junit.Assert;
// import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;
import java.text.DecimalFormat;

 /**
  * A JUnit test file to test the methods in the Truck class.
  */
public class TruckTest {

      
   static final double TAX_RATE = 0.02, 
                              ALTERNATIVE_FUEL_TAX_RATE = 0.01, 
                              LARGE_TRUCK_TONS_THRESHOLD = 2.0, 
                              LARGE_TRUCK_TAX_RATE = 0.03;    


   /** Fixture initialization (common initialization
    *  for all tests). **/
   @Before public void setUp() {
   }

   /** Test getTons method in Vehicle.  
    * @throws NegativeValueException from scanning negative input data.
    */
   @Test public void getTonsTest() throws NegativeValueException {
      
      Truck truck1 = 
         new Truck("Williams, Jo", "2012 Chevy Silverado", 30000, false, 1.5);
                  
      Assert.assertEquals("getTons Passed", 1.5, truck1.getTons(), 0.000001);
   }
   
   /** Test setTons method in Vehicle.  
    * @throws NegativeValueException from scanning negative input data.
    */
   @Test public void setTonsTest() throws NegativeValueException {
      
      Truck truck1 = 
         new Truck("Williams, Jo", "2012 Chevy Silverado", 30000, false, 1.5);
      
      truck1.setTons(2.0);
                              
      Assert.assertEquals("setTons Passed", 2.0, truck1.getTons(), 0.000001);
   }
   
   /** Test toString method in Truck.  
    * @throws NegativeValueException from scanning negative input data.
    */
   @Test public void toStringTest() throws NegativeValueException {
   
                     
      String output = "";
      DecimalFormat df = new DecimalFormat("$#,##0.00");
      
   /** Test truck with no alternative fuel  
    * @throws NegativeValueException from scanning negative input data.
    */
      Truck truck1 = 
         new Truck("Williams, Jo", "2012 Chevy Silverado", 30000, false, 1.5);
                    
      output = truck1.getOwner() + ": " + truck1.getClass().getName() 
             + " " + truck1.yearMakeModel;
         
      output += "\n" + "Value: " + df.format(truck1.getValue()) 
             + " Use Tax: " + df.format(truck1.useTax());
      
      output += "\n" + "with Tax Rate: " + TAX_RATE;
                  
      Assert.assertEquals("toString Passed", output, truck1.toString());
   
                  
   
   }    
   
   /** Test toString method in Truck with Alt Fuel & Threshold.  
    * @throws NegativeValueException from scanning negative input data.
    */
   @Test public void toStringAltFuelTest() throws NegativeValueException {
   
      String outputAltFuel = "";
      DecimalFormat df = new DecimalFormat("$#,##0.00");
         
   
      /** Test car with alternative fuel & luxury threshold */
      Truck truck2 = 
         new Truck("Williams, Sam", "2010 Chevy Mack", 40000, true, 2.5);
      
      
      outputAltFuel = truck2.getOwner() + ": " + truck2.getClass().getName() 
             + " " + truck2.yearMakeModel + " (Alternative Fuel)";
         
      outputAltFuel += "\n" + "Value: " + df.format(truck2.getValue()) 
             + " Use Tax: " + df.format(truck2.useTax());
      
      outputAltFuel += "\n" + "with Tax Rate: " + ALTERNATIVE_FUEL_TAX_RATE;
   
      outputAltFuel += " Large Truck Tax Rate: " + LARGE_TRUCK_TAX_RATE;
                             
   
                  
      Assert.assertEquals("toString Passed", outputAltFuel, truck2.toString());
      
   }

   /** Test useTax method.  
    * @throws NegativeValueException from scanning negative input data.
    */
   @Test public void useTaxTest() throws NegativeValueException {
   
      Truck truck2 = 
         new Truck("Williams, Sam", "2010 Chevy Mack", 40000, true, 2.5);
      
      Assert.assertEquals("useTax test", 1600.0, truck2.useTax(), 0.000001);
   
   }              

}
