## Homework #4

Author:	Karn Kausal

Class:		MET CS 622

Date:		6/6/2021



## Description

- This program creates random genome sequences where each sequence is 10 character  long, e.g. ATGGCAACAG, CAACATCAGC, TTCTCTGTTT.
- After creating the sequences, the program is run **with** concurrency/multi-threads and **without** threads/concurrency  to observe if the program runs faster. For example,  for a 100 genome sequence, we can use concurrency and use five threads to create 20 genome sequence each. 

## Assumptions

Following assumptions were made during the design and development phase:

- It was assumed acceptable to apply Object-Oriented Programming concepts and develop individual classes as needed:
  - **Genome** class:
    - No getters/setters because fields are "final" and constants.
- It was assumed that getters/setters are not required for the MultiThread.java class that implements the run() method.  This is similar to Warren's session where on slide 19 of Week5 slides, the private member variables did not have a getter/setter.  This also produced clean and readable code.
- SecureRandom was used to ensure random numbers generated are non-deterministic, unlike the Random class which can produce deterministic/predictable random numbers.   Online research on various websites indicated that SecureRandom should also be considered from a security standpoint as recommended by NIST.
- StringBuffer was researched as it is thread-safe.  However, most online data indicated that unless there is a need to use a mutable string across multiple threads, StringBuilder is better as it is a bit faster than StringBuffer.  Since this homework has no requirement for synchronization, StringBuilder was assumed to be acceptable.
- Given the complexity of concurrency, it was assumed that the focus should be kept on threads and instead of catching the InterruptedException, the main method simply declares it in the method header.

## Video Presentation

- A short zoom recording with audio commentary is included with the following format:
  - The video starts with a set of PowerPoint slides that includes class diagram as well as contains slides for most of the classes along with code screenshots.  
  - A screenshot of the final output is also shown and explained in detail.  
  - A live demo is provided towards the end of the video.  
  - Typo:  The Assumptions slide shown in the video has a typo where it says "there is one additional class compared to the previous program".  Please ignore this statement.  It was a copy & paste error.  

## Instructions to run the program

- The video presentation above has detailed instructions.  

- The Hw4_IntelliJ folder contains the source code folder (src) to execute the program.  