package pkg.hw4.multithread;

import java.util.ArrayList;

/**
 *  A main driver class with multi-threading or concurrency.
 */
public class MultiThreadMain {

    public static void main(String[] args) throws InterruptedException {

        // number of threads to be generated
        final int NUM_OF_THREADS = 10;

        // track the time in milliseconds to determine how long it takes to
        // to generate random genome sequences
        long startTime      = 0;
        long endTime        = 0;
        long elapsedTime    = 0;

        // manage threads inside an array list to allow loop traversal later
        ArrayList<Thread> threadArrayList = new ArrayList<>();

        // create the threads
        for (int i = 0; i < NUM_OF_THREADS; i++) {
            threadArrayList.add(new Thread(new MultiThread(i+1)));
        }
        // start the clock
        startTime = System.currentTimeMillis();

        // start the threads
        for (int i = 0; i < NUM_OF_THREADS; i++) {
            System.out.println("Starting Thread: " + (i + 1));
            threadArrayList.get(i).start();
        }
        // join all threads to ensure the elapsed time prints after all threads
        // have stopped.
        for (Thread thread : threadArrayList) {
            thread.join();
        }

        // stop the clock and record time in milliseconds
        endTime = System.currentTimeMillis();

        elapsedTime = endTime - startTime;
        System.out.println("Elapsed time in milliseconds: " + elapsedTime);

    } // end main method
} // end class
