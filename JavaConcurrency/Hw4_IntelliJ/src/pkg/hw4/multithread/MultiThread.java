package pkg.hw4.multithread;

import pkg.hw4.Genome;

/**
 * Implements multi-threading/concurrency.
 * The run() method "creates" genome sequences via the Genome object.
 *
 * @author Karn Kausal
 */

public class MultiThread implements Runnable {

    // track the thread name to print to the console
    private int ThreadID;

    // constant for number of random sequences
    private final int NUM_OF_RANDOM_SEQUENCES = 10;

    // create genome object that will be used to create a single sequence
    // that is 10 characters long
    private Genome genome = new Genome();

    /**
     * Constructor that receives the ID or name of the thread so that it
     * can be displayed to the console.
     * @param threadID thread's ID or name as provided by main()
     */
    public MultiThread(int threadID) {
        this.ThreadID = threadID;
    }

    /**
     * Implement the Runnable interface's method so that each thread
     * creates the sequence using concurrency.
     */
    @Override
    public void run() {
        // create the genome sequences and print them to the console
        for (int i = 0; i < NUM_OF_RANDOM_SEQUENCES; i++) {
            String sequence = genome.createSequence();
            System.out.println("Sequence " + (i + 1)
                                + " from Thread "
                                + this.ThreadID + ": " + sequence);
        } // end for loop

    } //end run method
} // end class
