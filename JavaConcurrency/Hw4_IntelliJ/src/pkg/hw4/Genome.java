package pkg.hw4;

import java.security.SecureRandom;

/**
 * This class satisfies the following requirements:
 * Creates a program that creates random genome sequences and each should
 * be 10 character long, e.g. ATGGCAACAG,CAACATCAGC,TTCTCTGTTT
 *
 * Note:  Genome sequence is a character string that include A,T,G,C.
 *
 * @author Karn Kausal
 */

public class Genome {

    /* ************************* Constants ************************* */


    // constants above will be mapped to the genome letter where:
    // genomeSequence[0] = A, genomeSequence[1] = B, etc
    private final String[] GENOME_SEQUENCE    = {"A", "T", "G", "C"};

    // genome length
    private final int GENOME_LENGTH = 10;

    // a function that is called 100 times in a single thread.
    public String createSequence() {

        // create a secure random number
        SecureRandom secureRandom = new SecureRandom();
        // string object used to build the 10 character genome sequence
        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < GENOME_LENGTH; i++) {
            // create a random number generated using secure random in the
            // range 0 (inclusive) and 4 (exclusive). The random number
            // will be in the range of 0 through 3.
            int randomValue = secureRandom.nextInt(4);

            // use the randomValue as the index number of the string array
            // that holds the genome sequence and then append the array
            // element to build a string of length 10.
            sb.append(GENOME_SEQUENCE[randomValue]);
        } // end loop

        return sb.toString();
    } // end createSequence method
}
