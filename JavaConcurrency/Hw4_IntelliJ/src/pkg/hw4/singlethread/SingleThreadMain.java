package pkg.hw4.singlethread;

import pkg.hw4.Genome;

/**
 *  No threads are used for this class because a
 *  single thread program is simply a program with a main thread (with no
 *  multi-threading or concurrency.
 */
public class SingleThreadMain {

    public static void main(String[] args) {

        // number of random sequences to be generated
        final int NUM_OF_RANDOM_SEQUENCES = 100;

        // track the time in milliseconds to determine how long it takes to
        // to generate random genome sequences
        long startTime      = 0;
        long endTime        = 0;
        long elapsedTime    = 0;

        // create genome object that will be used to create a single sequence
        // that is 10 characters long
        Genome genome = new Genome();

        // start the clock
        startTime = System.currentTimeMillis();

        // create random genome sequences
        for (int i = 0; i < NUM_OF_RANDOM_SEQUENCES; i++) {
            String sequence = genome.createSequence();
            // print sequence to the console
            System.out.println("Sequence " + (i + 1) + ": " + sequence);
        }
        // stop the clock and record time in milliseconds
        endTime = System.currentTimeMillis();
        elapsedTime = endTime - startTime;

        System.out.println("Elapsed time in milliseconds: " + elapsedTime);

    } // end main method
} // end class
