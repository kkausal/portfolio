package pkg.hw4IO.singlethreadIO;

import pkg.hw4.Genome;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

/**
 *  No threads are used for this class because a
 *  single thread program is simply a program with a main thread (with no
 *  multi-threading or concurrency).
 *  Implemented with I/O (by writing to an output file).
 */
public class SingleThreadMainIO {

    public static void main(String[] args) {

        // number of random sequences to be generated
        final int NUM_OF_RANDOM_SEQUENCES = 100;

        // track the time in milliseconds to determine how long it takes to
        // to generate random genome sequences
        long startTime      = 0;
        long endTime        = 0;
        long elapsedTime    = 0;

        // create genome object that will be used to create a single sequence
        // that is 10 characters long
        Genome genome = new Genome();

        // create the genome sequences, print them to the console as well as
        // write them to a csv file.
        try (BufferedWriter writer = new BufferedWriter(new FileWriter("outputSingleThread.csv"))) {
            // start the clock
            startTime = System.currentTimeMillis();

            // create random genome sequences
            for (int i = 0; i < NUM_OF_RANDOM_SEQUENCES; i++) {
                System.out.print("Sequence " + (i + 1) + " :");
                String sequence = genome.createSequence();
                // print sequence to the console as well as write to the file
                System.out.println(sequence);
                writer.write(sequence);
                writer.newLine();
            }
        } catch (IOException ioException) {
            System.out.println("IOException, cannot write to file.  Try again!");
        }
        // stop the clock and record time in milliseconds
        endTime = System.currentTimeMillis();
        elapsedTime = endTime - startTime;

        System.out.println("Elapsed time in milliseconds: " + elapsedTime);

    } // end main method
} // end class
