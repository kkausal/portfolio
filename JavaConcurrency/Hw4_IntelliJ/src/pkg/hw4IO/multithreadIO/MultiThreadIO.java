package pkg.hw4IO.multithreadIO;

import pkg.hw4.Genome;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Implements multi-threading/concurrency with I/O (by writing to an output file)
 * The run() method "creates" genome sequences via the Genome object.
 *
 * @author Karn Kausal
 */
public class MultiThreadIO implements Runnable {

    // track the thread name to print to the console
    private int ThreadID;

    // constant for number of random sequences
    private final int NUM_OF_RANDOM_SEQUENCES = 10;

    // create genome object that will be used to create a single sequence
    // that is 10 characters long
    private Genome genome = new Genome();

    /**
     * Constructor that receives the ID or name of the thread so that it
     * can be displayed to the console.
     * @param threadID thread's ID or name as provided by main()
     */
    public MultiThreadIO(int threadID) {
        this.ThreadID = threadID;
    }

    @Override
    public void run() {

        // create filenames appended with thread ID in the filename
        // example: outputThread-1.txt, outputThread-2.txt, etc.
        String fileName = "output" + this.ThreadID;

        // create the genome sequences, print them to the console as well as
        // write them to a csv file.
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(fileName))) {

            // create random genome sequences
            for (int i = 0; i < NUM_OF_RANDOM_SEQUENCES; i++) {
                String sequence = genome.createSequence();
                // print sequence to the console as well as write to the file
                System.out.println("Sequence " + (i + 1)
                                    + " from Thread "
                                    + this.ThreadID + ": " + sequence);
                writer.write(sequence);
                writer.newLine();
            }
        } catch (IOException ioException) {
            System.out.println("IOException, cannot write to file.  Try again!");
        }

    } //end run method
}
