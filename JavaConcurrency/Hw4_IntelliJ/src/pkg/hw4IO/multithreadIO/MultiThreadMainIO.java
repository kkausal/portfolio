package pkg.hw4IO.multithreadIO;

import java.util.ArrayList;

/**
 * A main driver with multi-threading or concurrency with IO.
 */
public class MultiThreadMainIO {

    public static void main(String[] args) throws InterruptedException {

        // number of random sequences to be generated
        final int NUM_OF_THREADS = 10;

        // track the time in milliseconds to determine how long it takes to
        // to generate random genome sequences
        long startTime      = 0;
        long endTime        = 0;
        long elapsedTime    = 0;

        // manage threads inside an array list to allow loop traversal later
        ArrayList<Thread> threadArrayList = new ArrayList<>();

        // create the thread
        for (int i = 0; i < NUM_OF_THREADS; i++) {
            threadArrayList.add(new Thread(new MultiThreadIO(i+1)));
        }

        // start the clock
        startTime = System.currentTimeMillis();

        // start the threads
        for (int i = 0; i < NUM_OF_THREADS; i++) {
            System.out.println("Starting Thread: " + (i + 1));
            threadArrayList.get(i).start();
        }

        // join all threads to ensure the elapsed time prints after all threads
        // have stopped.
        for (Thread thread : threadArrayList) {
            thread.join();
        }

        // stop the clock and record time in milliseconds
        endTime = System.currentTimeMillis();

        elapsedTime = endTime - startTime;
        System.out.println("Elapsed time in milliseconds: " + elapsedTime);

    } // end main method
} // end class
