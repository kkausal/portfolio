/**
 * MET CS 526
 * Term Project:
 * Process.java implements the "process" object
 * Note:  In the main simulation (ProcessScheduling.java), each entry in the
 * priority queue keeps a "process" object, which represents a process.
 *
 * @author Karn Kausal
 */

public class Process implements Comparable<Process> {

    /* ******************** Instance Variables ************************* */


    Integer pr;             // priority of the process
    Integer id;             // process id
    Integer arrivalTime;    // the time when the process arrives at the system
    Integer duration;       // execution of the process takes this amount of time
    Integer waitTime;       // "calculated" wait time a process has been waiting
    Integer timeInQueue;    // once a process has been added, track if its time in queue
                            // has exceeded the predetermined max wait time.


    /* ******************** Methods ************************************ */

    /**
     * Constructor to initialize the object.
     */
    public Process() {
        this.pr             = 0;
        this.id             = 0;
        this.arrivalTime    = 0;
        this.duration       = 0;
        this.waitTime       = 0;
        this.timeInQueue    = 0;

    }

    /**
     * Neatly prints the Process object.
     * @return String representation of the Process object.
     */
    public String toString() {
        String output = "";
        output =    "Id =  " + this.id + ", "               +
                    "priority = " + this.pr  + ", "         +
                    "duration = " + this.duration + ", "    +
                    "arrival time = " + this.arrivalTime;
        return  output;
    }

    /**
     * Override the natural ordering of the inherited compareTo and provides
     * ordering per arrival time.
     * @param p the received process object
     * @return integer value based on the arrival time of the received
     * value compared with the arrival time of the current process
     */
    @Override
    public int compareTo(Process p) {
        if (this.arrivalTime < p.arrivalTime ) {
            return -1;
        }
        else if (this.arrivalTime > p.arrivalTime) {
            return 1;
        }
        else {
            return 0;
        }
    }
}
