import java.util.Comparator;

public class PriorityComparator implements Comparator<Process> {

    /**
     * Orders based on smallest priority
     * @param p1 is a process object with a priority
     * @param p2 is a process object with a priority
     * @return 0 if p1 priority = p2 priority, -1 if p1 priority < p2 priority
     * and 1 if p1 priority > p2 priority
     */
    public int compare(Process p1, Process p2) {
        // if process 1 priority is smaller than process 2 priority
        if (p1.pr < p2.pr) {
            return -1;
        }
        // if process 1 priority is greater than process 2 priority
        else if (p1.pr > p2.pr) {
            return 1;
        }
        // if process 1 priority is equal to process 2 priority
        else {
            return 0;
        }
    }
}
