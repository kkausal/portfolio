import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.PriorityQueue;
import java.util.Scanner;

/**
 * MET CS 526
 * Term Project:
 * ProcessScheduling.java implements the following:
 * This project is an implementation of a small simulation program.
 * It is a simulation of a process scheduler of a computer system.
 * This simulated scheduler is a very small, simplified version,
 * which reflects some of the basic operations of a typical process scheduler.
 *
 * @author Karn Kausal
 */

public class ProcessScheduling {

    public static void main(String[] args) throws FileNotFoundException {

        //constants
        final int MAX_WORDS_PER_LINE = 10;
        final int MAX_WAIT_TIME = 30;

        // string to display info about removed processes
        String output = "";

        // an array of string to process each line of text file
        String[] line = new String[MAX_WORDS_PER_LINE];

        // stores the process w/ smallest priority removed from queue
        Process smallestPr = new Process();

        // an array list data structure to store processes
        // Note:  the project instructions called this data structure "D"
        // the remainder of the code will use the term D or processList interchangeably
        ArrayList<Process> processList = new ArrayList<>();

        // Initialize currentTime, finishTime, totalWaitTime & another
        // entry timeListEmpty to record the exact time when the data structure or ArrayList
        // goes empty
        int currentTime = 0, finishTime = 0, timeListEmpty = 0;
        double totalWaitTime = 0.0;

        // used to indicate whether the system is currently executing a process or not
        boolean running = false;

        // track whether or not all processes from the data structure "processList" have
        // been added to the queue.
        boolean allProcessesAdded = false;

        // track total processes to later compute average wait time
        int totalProcesses = 0;

        // create an empty priority queue Q
        PriorityQueue<Process> prQ = new PriorityQueue<>(new PriorityComparator());

        // object to write to output file
        PrintWriter outputFile = null;

        // open file for writing output
        String filename = "process_scheduling_output.txt";
        outputFile = new PrintWriter(filename);


        // Read all processes from an input file and store them in an
        // appropriate data structure called processList
        try {
            Scanner input = new Scanner(new File("process_scheduling_input.txt"));

            while (input.hasNext()) {
                // each line of file has four integers separated by a space(s).
                // four integers are: process id, priority, duration & arrival time
                line = input.nextLine().split(" ");

                // create a process from the line scanned from text file
                Process p = new Process();
                p.id = Integer.parseInt(line[0]);
                p.pr = Integer.parseInt(line[1]);
                p.duration = Integer.parseInt(line[2]);
                p.arrivalTime = Integer.parseInt(line[3]);

                // add process to the ArrayList
                processList.add(p);
                totalProcesses++;
            }
        }
        catch (FileNotFoundException e) {
            System.out.println("File not found!");

        }
        // sort the process list on arrival time
        // Note: Collections.sort method invokes the compareTo method in the Process object (Process.java)
        Collections.sort(processList);

        // print all processes first
        for (Process process : processList) {
            outputFile.println(process);
            System.out.println(process);
        }



        // Each iteration of the while loop represents what occurs during one time unit
        // while processList is not empty
        while (!processList.isEmpty()) {

            // display max wait time only once
            if (currentTime == 0) {
                output = "\nMaximum wait time = " + MAX_WAIT_TIME;
                outputFile.println(output);
                System.out.println(output);
            }

            // Get (don’t remove) a process p from "processList" that has the earliest arrival time
            // If the arrival time of p <= currentTime,
            // remove p from list of processes and insert it into Q
            // Note: Since process list is already sorted on arrival time,
            // the element at index 0 always has the smallest arrival time.
            if (processList.get(0).arrivalTime <= currentTime) {
                Process removedProcess = processList.remove(0);
                prQ.add(removedProcess);
            }

            // if a process is running & current time is equal to finish time
            // of process, stop executing the process.
            // Since process has finished execution, also update priority if
            // a process has been waiting longer than max wait time.
            if (running && (currentTime == finishTime)) {
                // display time when process finished executing
                output = "Process " + smallestPr.id +
                        " finished at time " + currentTime + "\n";
                outputFile.println(output);
                System.out.println(output);
                running = false;

                // update priorities:  if any process in queue has been
                // waiting longer than max wait time, decrease priority value
                // of that process by one
                System.out.println("Update Priority: ");
                outputFile.println("Update Priority: ");

                for (Process process : prQ) {
                    if (process.timeInQueue > MAX_WAIT_TIME) {
                        output = "PID = " + process.id + ", wait time = " + process.timeInQueue + " current priority = " + process.pr;
                        outputFile.println(output);
                        System.out.println(output);

                        // priority cannot be below 1 and so decrease
                        // only if greater than one.
                        if (process.pr > 1) {
                            process.pr = process.pr - 1;
                        }

                        output =  "PID = " + process.id + ", new priority = " + process.pr;
                        System.out.println(output);
                        outputFile.println(output);
                    }
                }
            } // end if clause checking current time with finish time

            // If Q is not empty and no process is running or being executed,
            // remove a process with the smallest priority from Q & execute
            // Calculate the wait time of the process
            // Set a flag running to true to mark execution of process
            if (!prQ.isEmpty() && !running) {
                smallestPr = prQ.remove();
                smallestPr.waitTime = currentTime - smallestPr.arrivalTime;
                running = true;

                // compute when current process will finish execution
                finishTime = currentTime + smallestPr.duration;

                // compute total wait time and print the removed process &
                // write to output file
                totalWaitTime = totalWaitTime + smallestPr.waitTime;
                output = printProcess(smallestPr, currentTime, totalWaitTime);
                outputFile.println(output);

            }

            // increment wait time of each process in queue
            if (!prQ.isEmpty()) {
                for (Process processInQ : prQ) {
                    processInQ.timeInQueue++;
                }
            }

            // record when process list or data structure processList
            // is all empty
            if (processList.size() == 0) {
                timeListEmpty = currentTime;
                allProcessesAdded = true;
            }

            // increment current time at end of each iteration
            currentTime++;

        } // end while loop


        // At this time all processes in processList have been moved to Q.
        // Execute all processes that are still in Q, one at a time.
        // When the execution of a process is completed, priorities of some processes
        // must be updated as needed.

        //While there is a process waiting in Q
        //Remove a process with the smallest priority from Q and execute it
        while (!prQ.isEmpty() || running) {
            // while process executes, if current time is equal to duration
            // of process, stop executing the process
            if (running && (currentTime == finishTime)) {
                // report the time when all processes were added to priority
                // queue and the data structure D or processList became empty
                if (allProcessesAdded) {
                    output = "\nD becomes empty at time "
                            + timeListEmpty + "\n";
                    outputFile.println(output);
                    System.out.println(output);
                    allProcessesAdded = false;
                }

                // display time when process finished executing
                output =  "Process " + smallestPr.id +
                        " finished at time " + currentTime + "\n";
                outputFile.println(output);
                System.out.println(output);
                running = false;

                // update priorities:  if any process in queue has been
                // waiting longer than max wait time, decrease priority value
                // of that process by one
                System.out.println("Update Priority: ");
                outputFile.println("Update Priority: ");

                for (Process process : prQ) {
                    if (process.timeInQueue > MAX_WAIT_TIME) {
                        output =  "PID = " + process.id
                                + ", wait time = " + process.timeInQueue
                                + " current priority = " + process.pr;
                        outputFile.println(output);
                        System.out.println(output);

                        // priority cannot be below 1 and so decrease
                        // only if greater than one.
                        if (process.pr > 1) {
                            process.pr = process.pr - 1;
                        }

                        output =  "PID = " + process.id
                                + ", new priority = " + process.pr;
                        outputFile.println(output);
                        System.out.println(output);
                    } // end if
                } // end for loop
            } // end outer if clause that checks currentTime vs finishTime

            // If Q is not empty and the flag running is false
            // Remove a process with the smallest priority from Q
            // Calculate the wait time of the process
            // Set a flag running to true
            if (!prQ.isEmpty() && !running) {
                smallestPr = prQ.remove();
                smallestPr.waitTime = currentTime - smallestPr.arrivalTime;
                running = true;

                // compute when current process will finish execution
                finishTime = currentTime + smallestPr.duration;

                // compute total wait time and print the removed process and
                // write to the file
                totalWaitTime = totalWaitTime + smallestPr.waitTime;
                output = printProcess(smallestPr, currentTime, totalWaitTime);
                outputFile.println(output);
            }

            // increment wait time of each process in queue
            if (!prQ.isEmpty()) {
                for (Process processInQ : prQ) {
                    processInQ.timeInQueue++;
                }
            }

            // increment current time at end of each iteration
            currentTime++;

        } // end while loop traversing priority queue after process list became empty

        // display and write total wait time
        //System.out.println();
        output = "\nTotal wait time = " + totalWaitTime;
        outputFile.println(output);
        System.out.println(output);

        // calculate, display and write avg wait time
        output = "Average wait time = " + totalWaitTime/totalProcesses;
        outputFile.println(output);
        System.out.println(output);

        // close output file
        outputFile.close();

    } // end main

    /**
     * Neatly prints the removed process information
     * @param smallestPr the smallest priority
     * @param currentTime the current logical time of the simulation
     * @param totalWaitTime the total wait time of the process
     * @return String that is displayed to console & written to file
     */
    private static String printProcess(Process smallestPr, int currentTime, double totalWaitTime) {
        String output;
        // display process & queue information
        output =    "\nProcess removed from queue is: id = " + smallestPr.id
                    + ", at time "              + currentTime
                    + ", wait time = "          + smallestPr.waitTime
                    + " Total wait time = "     + totalWaitTime
                    + "\nProcess id = "         + smallestPr.id
                    + "\n\t\tPriority = "       + smallestPr.pr
                    + "\n\t\tArrival = "        + smallestPr.arrivalTime
                    + "\n\t\tDuration = "       + smallestPr.duration;


        System.out.println(output);
        return output;
    } // end method

} // end class
