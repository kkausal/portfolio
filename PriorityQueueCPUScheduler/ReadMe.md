## Priority Scheduler

Author:		Karn Kausal

Class:		MET CS 526



## Description

This project is an implementation of a small simulation program. It is a simulation of a process scheduler of a computer system. 
This simulated scheduler is a very small, simplified version, which reflects some of the basic operations of a typical process scheduler. 
Detail description is listed in the project-assignment.pdf file.

## Analysis and Observations

Analysis and observations are included in the kausal_term_project_observations.pdf file.

## Psuedocode 

- Read all processes from an input file and store them in an appropriate data structure, D 
- Initialize currentTime
- running = false
- create an empty priority queue Q

// Each iteration of the while loop represents what occurs during one time unit

// Must increment currentTime in each iteration

- While D is not empty // while loop runs once for every time unit until D is empty
  - Get (don’t remove) a process p from D that has the earliest arrival time 
  - If the arrival time of p <= currentTime
  - Remove p from D and insert it into Q
  - If Q is not empty and the flag running is false

- Remove a process with the smallest priority from Q 
  - Calculate the wait time of the process
  - Set a flag running to true
  - If currently running process has finished
  - Set a flag running to false
  - Update priorities of processes that have been waiting longer than max. wait time

- End of While loop

// At this time all processes in D have been moved to Q.

// Execute all processes that are still in Q, one at a time.

// When the execution of a process is completed, priorities of some processes 

// must be updated as needed.

- While there is a process waiting in Q
  - Remove a process with the smallest priority from Q and execute it
- Calculate average wait time.  

## Instructions to run the program

- Compile and run the main method in the ProcessScheduling.java.  
- The ProcessScheduling.java executes process_scheduling_input.txt.
- The TermProject_IntelliJ folder contains the source code folder (src).  