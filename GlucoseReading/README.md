# **Blood Glucose Reading Log**

Karn Kausal

Class: CS 521 - 2021 Spring 1

Date: February 20, 2021



**Why is the program useful?**

I take care of my disabled & diabetic father who struggles to maintain a blood glucose log as required for his doctor visits. This program is useful as it will allow diabetic patients to log their blood glucose reading, date, time,  insulin and have the results stored in a file to be shared with their physician. 

**What the program does?**

The program will:

- take input from the user in the following format (separated by commas): date, reading, insulin, time.
- the input data will be processed in a dictionary and stored in an external output file when user exits the 
  program.
- As user adds new entries, user added entries will be displayed as a log table for visual feedback to show what was added. This same data is then written to the file as well before the program ends.
- Since user can add multiple time entries (M, A, E) for a given date, a unique key was created by
  concatenating date & time (e.g. "1/1/2021M or 12/1/2021A"). Also, using dictionary allows user to 
  change existing entries if they entered incorrect glucose readings for a given date. For example, if they 
  entered:
  `1/1/2021, 149, 25, M`
  To change 149 to 189, the user would retype the entry as : `1/1/2021, 189, 25, M`
- For the values of the dictionary, the structure is as follows:
  {date1 : value1, date2: value2, ….}, where each value is a **GlucoseReading** object with attributes date, 
  reading, insulin, and time.

**Instructions to run the program:**

- Run the main module “final_project.py”.
- Read the instructions on the screen in the welcome message and enter the user data per the instructions.
- As an example, if you enter the following and press Enter,
  `1/1/2021, 149, 25, M`
  The program will show you the newly added entry and asks whether or not you want to log another 
  glucose reading. If you enter Y, the process repeats. If you enter N, the program ends and all entries will be saved in a text file titled “GlucoseReading.txt” in the same local folder as the program file. The 
  program informs the user about the output file before it exits.
- Note: If the file does not exist, the program creates it. If the file already exists, the program will append 
  to the existing log because we do not want to erase their previous glucose reading logs.