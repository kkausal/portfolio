class GlucoseReading(object):
    """ A Glucose reading object w/ Date, Glucose Reading, Insulin & Time. """

    def __init__(self, date="", reading=0, insulin=0, time=""):
        """constructor, instantiate a new Glucose Reading. """

        # private attributes
        self.__date = date

        # public attributes
        self.reading = int(reading)
        self.insulin = int(insulin)
        self.time = time

    # since date is used as a key for dictionary in main program, it is
    # private given the importance of this value in the program
    def __set_date(self, date):
        """Sets a new date as provided by the calling program. """

        self.__date = date

    def get_date(self):
        """Public method to return the date. """

        return self.__date

    def add_date(self, date):
        """Public method to add the date."""

        self.__set_date(date)

    def __repr__(self):
        """Representation for a Glucose Reading data."""

        return f'{self.__date:<10s} {self.reading:^12d} ' \
               f'{self.insulin:^12d} {self.time:^12s}\n'


if __name__ == "__main__":

    test_obj = GlucoseReading("1/1/2021", 149, 25, "M")

    # Test general methods.
    # Note:  testing add_date method also tests the __set_date() method but
    # implicitly.
    # call add_date() & then use get_date() to verify date added correctly.
    # Note: this also asserts/validates the get_date() method.
    test_obj.add_date("12/25/2021")
    assert test_obj.get_date() == "12/25/2021", "Retrieved date is incorrect!"

    # Test attributes.
    assert test_obj.reading == 149, "Incorrect reading."
    assert test_obj.insulin == 25, "Incorrect Insulin."
    assert test_obj.time == "M", "Incorrect time of day."
    print("Unit tests passed.")


