"""
Karn Kausal
Class: CS 521 - 2021 Spring 1
Date:  February 20, 2021
Final Project
Description of Problem (just a 1-2 line summary!):
Logs blood glucose readings and stores results in a file to be shared with
a physician.
"""
from GlucoseReading import GlucoseReading
import os

# constants for date elements
MIN_MONTH = 1
MAX_MONTH = 12
MIN_DATE = 1
MAX_DATE = 31
MIN_YEAR = 0
MAX_YEAR = 9999

# constant to validate user entered input
MAX_VALUES = 4


def welcome_message():
    """Prints a welcome message & instructions for the user. """
    print("Welcome to your Blood Glucose Log.")
    print("When entering data, please separate entries via commas.  "
          "'For example: 1/2/2021, 145, 25, M'")

    # instead of using /t, manual space tabbing was used.
    print("Please note the following: ")
    print("    First entry is date of your reading. (Include slashes as shown "
          "in example. NO EXTRA SLASHES)")
    print("    Second entry number is your blood sugar reading.")
    print("    Third entry number is your insulin value (in cc).")
    print("    Fourth entry must be time of day.  Enter M = Morning, "
          "A = Afternoon, E = Evening.  Upper or lower case is ok.")


def request_user_input():
    """ Request date, reading, insulin, time & return as list """

    print("\nPlease enter date, blood glucose, insulin & time "
          "(put commas between each entry as shown above): ")

    input_str_list = input().split(",")
    return input_str_list


def is_data_valid(_input_list):
    """Validate input. Return true if no error. Else, false & err msg. """

    datevalid_bool, err_str = False, ""

    # validate if user provided missing entries, which would include
    # empty or whitespace entry
    for user_entry in _input_list:
        if user_entry == "" or user_entry == " ":
            err_str = "One of your entries is blank/invalid. " \
                      "Please try again."
            # no point in validating the data if data is missing.
            return datevalid_bool, err_str

    # if user entered incorrect number of entries
    if len(_input_list) != MAX_VALUES:
        err_str = f'Oops!  Four entries (date, reading, insulin and ' \
                  f' time) are required. ' \
                  f'You typed {len(input_data_list)} entries.  ' \
                  f'Please try again.'

        # no point in validating the data if user did not provide all
        # required entries and so exit function.
        return datevalid_bool, err_str
    # if user provided the four required entries, process the data
    else:

        _mon_date_yr_str = _input_list[0]
        _blood_sugar_str = _input_list[1]
        _reading_str = _input_list[2]
        _time_str = _input_list[3]

        # extract date elements as integers and convert reading and blood
        # glucose/sugar to integers as well.
        # if error occurs, do not validate the data further and return.
        try:
            _month_str, _date_str, _year_str = _mon_date_yr_str.split("/")
            _month_int, _date_int, _year_int = \
                int(_month_str), int(_date_str), int(_year_str)

            # validate that blood glucose and insulin readings can be
            # converted to integers.  This ensures values can be added later
            # in the code.
            _blood_sugar_int = int(_blood_sugar_str)
            _reading_int = int(_reading_str)

        except ValueError as e:
            err_str = "\nInvalid entry.  Did you enter everything " \
                      "correctly?  Please check your entries and try again."

            # no point in validating the data if error occurred.
            # return false with error message
            return datevalid_bool, err_str

        # validate date and time elements
        if (MIN_MONTH <= _month_int <= MAX_MONTH
                and MIN_DATE <= _date_int <= MAX_DATE
                and MIN_YEAR <= _year_int <= MAX_YEAR):

            #err_str = None
            datevalid_bool = True

        # if date not valid, construct error message for date elements
        else:
            if not (MIN_MONTH <= _month_int <= MAX_MONTH):
                err_str = f'\nInvalid: there can be only {MAX_MONTH} ' \
                          f'months in a year.'
            if not (MIN_DATE <= _date_int <= MAX_DATE):
                err_str += f'\nInvalid: there can be only {MAX_DATE} ' \
                           f'days in a month.'
            if not (MIN_YEAR <= _year_int <= MAX_YEAR):
                err_str += f'\nInvalid: there can be only {MAX_YEAR} ' \
                           f'years.'

        # validate time of day. if invalid, generate error message.
        if _time_str.strip() not in "MAE":
            err_str += f'\nInvalid: time of day can only be M, A or E. ' \
                       f'Please try again.'
            datevalid_bool = False

    return datevalid_bool, err_str


def header_title():
    """As user enters data, format header title neatly & return as string. """

    usr_display_str = f'{"Date":<12s} {"Reading":<12s} {"Insulin":<12s} ' \
                      f'{"Time":<12s}\n'
    usr_display_str += f'{"--------":<12s} {"--------":<12s} {"--------":<12s} ' \
                       f'{"--------":<12s}\n'
    # instead of printing, return the string so that it can also be used to
    # write to the file.
    return usr_display_str

# start the main program
# track whether or not the user wants to quit the program
quit_bool = False

# track whether valid data was entered by the user
valid_bool = False

# if valid data was entered, store data in dictionary to write to file and
# display it.
data_dict = dict()

# if invalid data provided by user, track the error message
err_msg_str = ""

# display welcome message
welcome_message()

# while the user has not quit, keep the program running
while not quit_bool:

    # request data from user
    input_data_list = request_user_input()

    # validate data from the user
    valid_bool, err_msg_str = is_data_valid(input_data_list)

    # if data valid, store data for a new Glucose Reading
    if valid_bool:
        # create a new Glucose Reading instance per data received from user
        new_reading = GlucoseReading()
        # add date provided by patient/user
        new_reading.add_date(input_data_list[0].strip())
        #
        new_reading.reading = int(input_data_list[1])
        new_reading.insulin = int(input_data_list[2])
        new_reading.time = input_data_list[3].strip().upper()

        # generate unique key for dictionary.  Since user can add multiple
        # time entries (M, A, E) for a given date, create a unique key by
        # concatenating date & time (e.g. "1/1/2021M or 12/1/2021A")
        # Also, using dictionary allows user to change existing entries if
        # they entered incorrect entries for a given date.
        key = new_reading.get_date() + new_reading.time

        # add new glucose reading to dictionary
        data_dict[key] = new_reading

        # before writing data to file, display the data entered so far
        # in a neat format with a header title.
        # Note:  print calls repr() method.
        print(header_title())
        for key, each_reading in data_dict.items():
            print(each_reading)

        # inform user they can update existing entry, if needed
        print("Note: To update an already entered Reading or Insulin value, "
              "simply re-enter as before but with updated values.\n")
    # if invalid data was received from user, print the error message
    else:
        print(err_msg_str)

    # only ask for another entry if no error.  if error, go to the top and
    # display original message and ask user to enter again
    if valid_bool:
        quit_str = input("Do you want to log another entry? (Enter Y or N):  ")

        # while user has not entered a Y or N, keep asking if they want to
        # log another entry.
        while quit_str.upper() not in "YN":
            print("Invalid entry.  Type Y or N.")
            quit_str = input("Do you want to log another entry? "
                             "(Enter Y or N):  ")
        # if user wants to quit, inform them that log has been
        # written to the file and exit the program.
        if quit_str.lower() == 'n':
            quit_bool = True
            print("Writing data to file.")
            print("Your data has been written to 'GlucoseReadings.txt' in the"
                  " same folder as this program.")
        else:
            quit_bool = False


# get the current directory and create the filename
current_dir = os.getcwd()
filename = os.path.join(current_dir, "GlucoseReadings.txt")

# if file already exists and we are appending to it, do not rewrite the
# header.  Else, write the header.
if not os.path.isfile(filename):
    write_header_bool = True
else:
    write_header_bool = False

# open the file for appending data
file = open(filename, 'a')

# only write the header once if file is new and does not exist.
if write_header_bool:
    file.write(header_title())

# write patient's glucose readings to the file
for key, reading in data_dict.items():
    # since write method only accepts string, use repr() function to convert.
    # reference source:
    # https://www.geeksforgeeks.org/convert-object-to-string-in-python/
    file.write(repr(reading))

file.close()

